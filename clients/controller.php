<?php
/**
 ******************************** jlab-mvc *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/clients/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** jlab-mvc *******************************
 */

class Clients extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }

    public function index() {
        // check for view access
        if (Session::get('clients_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess');
            exit;
        } else {
            // continue to clients page if enough access
            $this->view->getTheme('admin');
            $this->view->heading = 'Clients';
            $this->view->renderHeader();
            $this->view->renderView('clients/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('clients/view/js/clients.js.php');
            $this->view->renderBottom();
        }
    }

}
