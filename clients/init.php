<?php
/**
 ******************************** jlab-mvc *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: init.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/clients/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** jlab-mvc *******************************
 */

$db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

// add initialization routine
// set the module name
$this_module = basename(__DIR__);

// check if any module role access permissions exist
if (!Utils::permissionsInDb($this_module)) {
    // add role permissions for developer (1) and admin (2) core roles
	$db->insert(DB_PREFIX . "access", array('roles_id' => '1', 'module' => 'clients', 'access' => 5));
    $db->insert(DB_PREFIX . "access", array('roles_id' => '2', 'module' => 'clients', 'access' => 4));
    // other permissions adding capability to come later in the form of a "permissions" module
}

// check if module row exists in modules table
if (!Utils::moduleInDb($this_module)) {
    // if no row in modules table add it
    $db->insert(DB_PREFIX . "modules", array(
        'module' => 'clients',
        'title' => 'Clients',
        'summary' => 'Clients Summary',
        'theme' => 'admin',
        'icon' => 'fa-users',
        'sort' => 0,
        'visibility' => 1,
        'status' => 0,
        'system' => 0
    ));
	$db->insert(DB_PREFIX . 'options', array(
		'define' => 'clientsAdminPagination',
		'title' => 'Admin Clients Listing Pagination',
		'summary' => 'The number of items to show per page in the admin Clients listing page.',
		'value' => 25,
		'type' => 'clients',
		'edit' => 1,
		'system' => 0,
		'system' => 1
	));
	$db->insert(DB_PREFIX . 'options', array(
		'define' => 'clientsAdminMenuSortOrder',
		'title' => 'Admin Clients Menu Sort Order',
		'summary' => 'Use this setting to control where in the Admin Menu the Clients menu listing is ordered.',
		'value' => 30,
		'type' => 'clients',
		'edit' => 1,
		'system' => 0,
		'system' => 1
	));
    $db->create("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "clients` (
		`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
		`active` tinyint(1) NOT NULL DEFAULT '1',
		`name` varchar(63) NOT NULL,
		`time_remaining` decimal(7,2) NOT NULL,
		`is_time_blocked` tinyint(1) NOT NULL,
		`time_block_change_date` datetime NOT NULL,
		`username` varchar(48) NOT NULL,
		`password` varchar(64) NOT NULL,
		PRIMARY KEY (`id`) USING BTREE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    );
    Session::setSessionAccess($_SESSION['usersRoleId']);
    header("Location: /modules");
}