<?php
/**
 ******************************** jlab-mvc *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/clients/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** jlab-mvc *******************************
 */

$_menu = array(
    'item' => '/clients',
    'title' => 'Clients',
    'system' => 0,
    'icon' => 'fa-street-view',
);
