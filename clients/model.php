<?php
/**
 ******************************** jlab-mvc *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/clients/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** jlab-mvc *******************************
 */

class clients_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

}
