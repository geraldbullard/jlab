<?php
/**
 ******************************** jlab-mvc *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/clients/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** jlab-mvc *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<div class="row">
    <div class="col-lg-12" id="clients-alert-message">
        <?php if (Session::get('clientsMessage') != '') { ?>
        <div class="alert alert-success alert-dismissible alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> <?php echo $Language->get(Session::get('clientsMessage')); ?></h4>
        </div>
        <?php Session::setNull('clientsMessage'); } ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-3 clients-add-button">
        <a href="/clients/createclient">
            <button class="btn btn-lg btn-primary" type="button">
                <i class="fa fa-plus"></i>
                &nbsp;Add New Client
            </button>
        </a>
    </div>
    <div class="col-xs-12 col-md-9 pull-right" id="clients-filter">
        <div class="box box-info filter-box collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Search Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <form class="form-horizontal" action="/clients/" method="get">
                <input type="hidden" name="clients_filter" value="1">
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="user_search" class="col-sm-3 control-label">Search</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="user_search" name="user_search"
                                               value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="user_role" class="col-sm-3 control-label">Role</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select id="user_role" name="user_role" class="form-control">
                                            <option value="">Select a Role</option>
                                            <option value="1">Developer</option>
                                            <option value="2">Admin</option>
                                            <option value="3">User</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="user_status" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select id="user_status" name="user_status" class="form-control">
                                            <option value="">Select a Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="/clients/">
                        <button type="button" class="btn btn-default">Reset</button>
                    </a>
                    <button type="submit" class="btn btn-info pull-right">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body no-padding">
                <table class="table">
                    <tbody>
                    <tr>
                        <th class="hide-below-1024">ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th class="hide-below-768">Email</th>
                        <th class="hide-below-1024">Username</th>
                        <th class="hide-below-1024">Role</th>
                        <th class="hide-below-1024">Status</th>
                        <th class="hide-below-1024">Created</th>
                        <th class="text-right actions-column">Actions</th>
                    </tr>
                    <tr><input type="hidden" name="roles_id" value="1">
                        <td class="hide-below-1024">1</td>
                        <td>Gerald</td>
                        <td>Bullard Jr</td>
                        <td class="hide-below-768">gerald.bullard@gmail.com</td>
                        <td class="hide-below-1024">owner</td>
                        <td class="hide-below-1024">Developer</td>
                        <td class="hide-below-1024">Active</td>
                        <td class="hide-below-1024">Aug 15, 2016</td>
                        <td>
                            <div class="pull-right btn-group">
                                <button class="btn btn-xs btn-primary" type="button"
                                        onclick="location.href='/user/edit/1'">
                                    <i class="fa fa-pencil"></i> <span class="hide-below-480">Edit</span>
                                </button>
                                <button class="delete-user btn btn-xs btn-danger" type="button" data-user-id="1">
                                    <i class="fa fa-trash-o"></i> <span class="hide-below-480">Delete</span>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr><input type="hidden" name="roles_id" value="2">
                        <td class="hide-below-1024">2</td>
                        <td>John</td>
                        <td>Tester</td>
                        <td class="hide-below-768">tabmin@localhost.com</td>
                        <td class="hide-below-1024">johntest</td>
                        <td class="hide-below-1024">Admin</td>
                        <td class="hide-below-1024">Active</td>
                        <td class="hide-below-1024">Apr 19, 2017</td>
                        <td>
                            <div class="pull-right btn-group">
                                <button class="btn btn-xs btn-primary" type="button"
                                        onclick="location.href='/user/edit/2'">
                                    <i class="fa fa-pencil"></i> <span class="hide-below-480">Edit</span>
                                </button>
                                <button class="delete-user btn btn-xs btn-danger" type="button" data-user-id="2">
                                    <i class="fa fa-trash-o"></i> <span class="hide-below-480">Delete</span>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr><input type="hidden" name="roles_id" value="3">
                        <td class="hide-below-1024">3</td>
                        <td>Test</td>
                        <td>User</td>
                        <td class="hide-below-768">me@mail.com</td>
                        <td class="hide-below-1024">testuser</td>
                        <td class="hide-below-1024">User</td>
                        <td class="hide-below-1024">Active</td>
                        <td class="hide-below-1024">May 04, 2018</td>
                        <td>
                            <div class="pull-right btn-group">
                                <button class="btn btn-xs btn-primary" type="button"
                                        onclick="location.href='/user/edit/3'">
                                    <i class="fa fa-pencil"></i> <span class="hide-below-480">Edit</span>
                                </button>
                                <button class="delete-user btn btn-xs btn-danger" type="button" data-user-id="3">
                                    <i class="fa fa-trash-o"></i> <span class="hide-below-480">Delete</span>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="displaying-div">
            Displaying 1 - 3 of 3 results
        </div>
    </div>
    <div class="col-sm-8">
        <div class="pagination-div">
            <ul class="pagination">
                <li class="paginate_button previous disabled" title="First Page"><a href="javascript:;"><i
                            class="fa fa-fast-backward"></i></a></li>
                <li class="paginate_button previous disabled"><a href="javascript:;" title="Previous Page"><i
                            class="fa fa-step-backward"></i></a></li>
                <li class="paginate_button active"><a href="javascript:;">1</a></li>
                <li class="paginate_button next disabled"><a href="javascript:;" title="Next Page"><i
                            class="fa fa-step-forward"></i></a></li>
                <li class="paginate_button next disabled"><a href="/user?page=0" title="Last Page"><i
                            class="fa fa-fast-forward"></i></a></li>
            </ul>
        </div>
    </div>
</div>
