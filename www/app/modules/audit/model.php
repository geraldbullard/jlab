<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/audit/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class audit_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function auditList($order = '', $limit = '') {
        $where = '';
        $whereArr = array();
        if (isset($_GET['audit_filter']) && $_GET['audit_filter'] > 0) {
            $where = ' WHERE ';
            $addTime = '';
            if ($_GET['audit_user'] != '') {
                $whereArr[] = '`users_id` = "' . $_GET['audit_user'] . '"';
            }
            if ($_GET['audit_date_range'] != '') {
                $dates = explode(" - ", $_GET['audit_date_range']);
                $sDate = new DateTime($dates[0]);
                $eDate = new DateTime($dates[1]);
                $whereArr[] = '`timestamp` BETWEEN "' . $sDate->format("Y-m-d") . ' 00:00:00" ';
                $whereArr[] = ' "' . $eDate->format("Y-m-d") . ' 23:59:59" ';
            }
            if ($_GET['audit_module'] != '') {
                $whereArr[] = '`module` = "' . $_GET['audit_module'] . '"';
            }
            $where = $where . implode(" AND ", $whereArr);
        }
        if ($order != '') {
            $order = ' ORDER BY `timestamp` ' . $order;
        } else {
            $order = ' ORDER BY `timestamp` DESC';
        }
        if ($limit != '') $limit = ' LIMIT ' . $limit;
        return $this->db->select("
            SELECT DISTINCT *
            FROM " . DB_PREFIX . "audit" .
            $where .
            $order .
            $limit
        );
    }
    
}
