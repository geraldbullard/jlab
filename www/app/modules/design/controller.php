<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/design/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Design extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }

    public function index() {
        // check for view access
        if (Session::get('design_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // continue to content listing page if enough access
            $this->view->getTheme('admin');
            $this->view->heading = 'Design';
            $this->view->renderHeader();
            $this->view->renderView('design/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('design/view/js/design.js.php');
            $this->view->renderBottom();
        }
    }

    public function menus() {
        // check for view access
        if (Session::get('design_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // continue to content listing page if enough access
            $this->view->getTheme('admin');
            $this->view->heading = 'Menus';
            $this->view->renderHeader();
            $this->view->renderView('design/view/menus');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('design/view/js/design.js.php');
            $this->view->renderBottom();
        }
    }

}
