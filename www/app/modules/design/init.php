<?php
/**
 ******************************** jlab-mvc *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: init.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/design/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** jlab-mvc *******************************
 */

$db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

// add initialization routine
// set the module name
$this_module = basename(__DIR__);

// check if any module role access permissions exist
if (!Utils::permissionsInDb($this_module)) {
    // add role permissions for developer (1) and admin (2) core roles
	$db->insert(DB_PREFIX . "access", array('roles_id' => '1', 'module' => 'design', 'access' => 5));
    $db->insert(DB_PREFIX . "access", array('roles_id' => '2', 'module' => 'design', 'access' => 4));
    // other permissions adding capability to come later in the form of a "permissions" module
}

// check if module row exists in modules table
if (!Utils::moduleInDb($this_module)) {
    // if no row in modules table add it
    $db->insert(DB_PREFIX . "modules", array(
        'module' => 'design',
        'title' => 'Design',
        'summary' => 'Design Summary',
        'theme' => 'admin',
        'icon' => 'fa-paint-brush',
        'sort' => 2,
        'visibility' => 1,
        'status' => 0,
        'system' => 0
    ));
	$db->insert(DB_PREFIX . 'options', array(
		'define' => 'designAdminMenuSortOrder',
		'title' => 'Admin Design Menu Sort Order',
		'summary' => 'Use this setting to control where in the Admin Menu the Design menu listing is ordered.',
		'value' => 10,
		'type' => 'design',
		'edit' => 1,
		'system' => 0,
		'system' => 1
	));
    $db->create("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "menus` (
		`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
		`active` tinyint(1) NOT NULL DEFAULT '1',
		`name` varchar(64) NOT NULL,
		`location` varchar(64) NOT NULL,
		PRIMARY KEY (`id`) USING BTREE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    );
    Session::setSessionAccess($_SESSION['usersRoleId']);
    header("Location: /modules");
}