<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/design/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
$_menu = array(
    'item' => '/design',
    'title' => 'Design',
    'icon' => 'fa-paint-brush',
    'system' => 1,
    'sub_items' => array(
        'sub_item' => array(
            'item' => '/design/menus/',
            'title' => 'Menus',
            'icon' => 'fa-bars',
            'system' => 1,
        ),
    ),
);