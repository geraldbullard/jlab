<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/design/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class design_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

}
