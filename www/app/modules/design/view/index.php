<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/design/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<?php if (Session::getStatusMessage('designMessage') != '') { ?>
<div class="row">
    <div class="col-lg-12" id="content-alert-message">
        <div class="alert alert-<?php echo Session::getStatusMessage('designMessage')['status']; ?> alert-dismissible alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $Language->get(Session::getStatusMessage('designMessage')['value']); ?></h4>
        </div>
    </div>
</div>
<?php Session::setNull('designMessage'); } ?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                Design/Appearance
            </div>
        </div>
    </div>
</div>