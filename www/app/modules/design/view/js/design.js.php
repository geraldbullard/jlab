<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: content.js.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/design/view/js/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<script>
    $(document).ready(function () {
        // Clear (fade out) session messages
        setTimeout(function() {
            $(".alert.alert-success.alert-dismissible").fadeOut("slow");
            $(".alert.alert-info.alert-dismissible").fadeOut("slow");
            $(".alert.alert-warning.alert-dismissible").fadeOut("slow");
            $(".alert.alert-danger.alert-dismissible").fadeOut("slow");
        }, 3000);
    });
</script>
