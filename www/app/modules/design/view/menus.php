<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: menus.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/design/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<?php if (Session::getStatusMessage('menusMessage') != '') { ?>
<div class="row">
    <div class="col-lg-12" id="content-alert-message">
        <div class="alert alert-<?php echo Session::getStatusMessage('menusMessage')['status']; ?> alert-dismissible alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $Language->get(Session::getStatusMessage('menusMessage')['value']); ?></h4>
        </div>
    </div>
</div>
<?php Session::setNull('menusMessage'); } ?>
<div class="row">
    <div class="col-xs-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#menus-tab" data-toggle="tab" aria-expanded="false">Edit Menus</a></li>
                <li><a href="#locations-tab" data-toggle="tab" aria-expanded="false">Manage Locations</a></li>
            </ul>
            <div class="tab-content">
                <!-- Menus Tab -->
                <div class="tab-pane active" id="menus-tab">
                    <section id="menus">
                        <div class="row">
                            <div class="col-sm-12">
                                Menu Selection Row
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <h4>Add Menu Items</h4>
                                <div class="box box-default box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"><?php echo $Language->get('text_pages'); ?></h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        Pages Stuff
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <h4>Menu Structure</h4>
                                <div class="box box-default box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Title</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                Structure Stuff
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- Locations Tab -->
                <div class="tab-pane" id="locations-tab">
                    <section id="locations">
                        <h4>Locations Tab</h4>
                        <div class="row">
                            <div class="col-sm-12">

                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>


<!--<div class="row">
</div>-->
