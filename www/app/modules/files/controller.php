<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/files/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Files extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }

    public function index() {
        // check for view access
        if (Session::get('files_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess');
            exit;
        } else {
            // continue to files page if enough access
            $this->view->getTheme('admin');
            $this->view->heading = 'Files';
            $this->view->renderHeader();
            $this->view->renderView('files/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('files/view/js/files.js.php');
            $this->view->renderBottom();
        }
    }

}
