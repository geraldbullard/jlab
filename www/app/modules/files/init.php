<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: init.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/files/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

$db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

// add initialization routine
// set the module name
$this_module = basename(__DIR__);

// check if any module role access permissions exist
if (!Utils::permissionsInDb($this_module)) {
    // add role permissions for developer (1) and admin (2) core roles
	$db->insert(DB_PREFIX . "access", array('roles_id' => '1', 'module' => 'files', 'access' => 5));
    $db->insert(DB_PREFIX . "access", array('roles_id' => '2', 'module' => 'files', 'access' => 4));
    // other permissions adding capability to come later in the form of a "permissions" module
}

// check if module row exists in modules table
if (!Utils::moduleInDb($this_module)) {
    // if no row in modules table add it
    $db->insert(DB_PREFIX . "modules", array(
        'module' => 'files',
        'title' => 'Files',
        'summary' => 'Files Summary',
        'theme' => 'admin',
        'icon' => 'fa-hdd-o',
        'sort' => 0,
        'visibility' => 1,
        'status' => 0,
        'system' => 0
    ));
    Session::setSessionAccess($_SESSION['usersRoleId']);
    header("Location: /modules");
}