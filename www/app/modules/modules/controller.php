<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/modules/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Modules extends Controller {

    public function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }
    
    public function index() {
        // check for view access
        if (Session::get('modules_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // continue to modules listing page if enough permission
            $this->model->init();
            $this->view->getTheme('admin');
            $this->view->heading = 'Modules';
            $this->view->renderHeader();
            $this->view->renderView('modules/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('modules/view/js/index.js.php');
            $this->view->renderBottom();
        }
    }

    public function edit($id = '') {
        // check for edit permissions
        if (Session::get('modules_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // check if id is empty
            if (empty($id)) {
                // redirect if id is empty
                header('Location: /modules/');
                exit;
            }
            // continue to user edit page if enough permission
            $this->view->settings = $this->model->modulesSingleList(Module::getModuleCodeById($id));
            $this->view->getTheme('admin');
            $this->view->heading = 'Edit Module Settings';
            $this->view->renderHeader();
            $this->view->renderView('modules/view/edit');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('modules/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }

    public function editsetting($id = '') {
        // check for edit permissions
        if (Session::get('modules_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // check if id is empty
            if (empty($id)) {
                // redirect if id is empty
                header('Location: /modules/');
                exit;
            }
            // continue to user edit page if enough permission
            $this->view->setting = $this->model->modulesSetting($id);
            $this->view->getTheme('admin');
            $this->view->heading = 'Edit Module Setting';
            $this->view->renderHeader();
            $this->view->renderView('modules/view/editsetting');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('modules/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }

    public function editsave($data = '') {
        // check for edit permissions
        if (Session::get('modules_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // save user if enough permission
            $postData = array();
            $postData['id'] = $_POST['id'];
            $postData['value'] = $_POST['value'];

            // @TODO: Do your error checking!

            if ($this->model->editsave($postData)) {
                header('location: /modules/edit/' . $_POST['module'] . '?setting=' . $_POST['id'] . '&msg=edit_success');
                exit;
            } else {
                header('location: /modules/edit/' . $_POST['module'] . '?setting=' . $_POST['id'] . '&msg=edit_failed');
                exit;
            }
        }
    }

    public function activate($id = '') {
        // check for activate/edit permissions
        if (Session::get('modules_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // activate module if enough permission
            $this->model->activate($id);
            header('location: /modules/');
            exit;
        }
    }

    public function deactivate($id = '') {
        // check for deactivate/edit permissions
        if (Session::get('modules_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // deactivate module if enough permission
            $this->model->deactivate($id);
            header('location: /modules/');
            exit;
        }
    }

    public function delete($id = '') {
        // check for delete permissions
        if (Session::get('modules_access') < ACCESS_DELETE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // delete module if enough permission
            $this->model->delete($id);
            header('location: /modules/');
            exit;
        }
    }

}
