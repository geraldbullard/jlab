<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: edit.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<form method="post" action="/modules/editsave/<?php echo $this->setting[0]['id']; ?>">
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $this->setting[0]['title']; ?>
                </h3>
            </div>
            <div class="panel-body">
                <input type="hidden" name="id" value="<?php echo $this->setting[0]['id']; ?>" />
                <input type="hidden" name="module" value="<?php echo $_GET['module']; ?>" />
                <p>
                    <label for="value">
                        <?php echo $Language->get('text_value'); ?>
                    </label>
                    <input
                        type="text"
                        name="value"
                        class="form-control"
                        value="<?php echo $this->setting[0]['value']; ?>" />
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <button type="submit" class="pull-left btn btn-lg btn-danger">
            <?php echo $Language->get('text_save'); ?>
        </button>
        <button
            type="button"
            class="edit-cancel pull-right btn btn-lg btn-default">
            <?php echo $Language->get('text_cancel'); ?>
        </button>
    </div>
</div>
</form>
