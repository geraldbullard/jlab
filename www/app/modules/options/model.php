<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class options_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function optionsList($type = '', $limit = '') {
        $where = '';
        $whereArr = array();
        if (isset($_GET['options_filter']) && $_GET['options_filter'] > 0) {
            $where = ' WHERE ';
            if ($_GET['options_search'] != '') {
                $termsLike = array();
                foreach (explode(" ", $_GET['options_search']) as $term) {
                    $termsLike[] = " `define` LIKE '%" . $term . "%' OR
                    `title` LIKE '%" . $term . "%' OR
                    `summary` LIKE '%" . $term . "%' OR
                    `value` LIKE '%" . $term . "%' ";
                }
                $whereArr[] = '(' . implode(' OR ', $termsLike) . ')';
            }
            if ($_GET['options_type'] != '') {
                $whereArr[] = '`type` = "' . $_GET['options_type'] . '"';
            }
            $where = $where . implode(" AND ", $whereArr);
        }
        if ($limit != '') $limit = ' LIMIT ' . $limit;
        return $this->db->select("
            SELECT DISTINCT *
            FROM " . DB_PREFIX . "options" .
            $where .
            $limit
        );
    }

    public function optionsSingleList($id) {
        return $this->db->select(
            'SELECT * FROM ' . DB_PREFIX . 'options WHERE ' . DB_PREFIX . 'options.id = :id',
            array(':id' => $id)
        );
    }

    public function create($data) {
        $result = $this->db->insert(DB_PREFIX . 'options', array(
            'define' => $data['define'],
            'title' => $data['title'],
            'summary' => $data['summary'],
            'value' => $data['value'],
            'type' => $data['type'],
            'edit' => $data['edit'],
            'system' => $data['system']
        ));

        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::RECORD_CREATED,
            Session::get('usersData'),
            'options/create/',
            'Option Created', // AuditTrail::loadTypeById(AuditTrail::RECORD_CREATED)->getTitle()
            $this->db->lastInsertId()
        );
        if ($result)
            return true;
        return false;
    }

    public function editsave($data) {
        global $Options;
        $postData = array(
            'id' => $data['id'],
            'define' => $_POST['define'],
            'title' => $_POST['title'],
            'summary' => $_POST['summary'],
            'value' => $_POST['value'],
            'type' => $_POST['type'],
            'edit' => $_POST['edit'],
            'system' => $_POST['system']
        );
        $result = $this->db->update(DB_PREFIX . 'options', $postData, "`id` = {$data['id']}");
        // add the success message to the session, and insert the audit trail db entry
        if ($result) {
            Session::setStatusMessage('optionMessage', 'text_option_save_success', 'success');
            // insert the autdit trail
            AuditTrail::log(
                AuditTrail::RECORD_EDITED,
                Session::get('usersData'),
                'options/edit/',
                'Option Edited', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
                $data['id']
            );
            return true;
        } else { // otherwise there was an error
            Session::setStatusMessage('optionMessage', $this->db->errorInfo(), 'danger');
            return false;
        }
    }

    public function delete($id) {
        $this->db->delete(DB_PREFIX . 'options', "`id` = '$id'");
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::RECORD_DELETED,
            Session::get('usersData'),
            'options/delete/',
            'Option Deleted', // AuditTrail::loadTypeById(AuditTrail::RECORD_DELETED)->getTitle()
            $id
        );
    }

}
