<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: create.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<form method="post" action="/options/create">
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $Language->get('text_new') . ' ' . $Language->get('text_option') . ' ' . $Language->get('text_information'); ?>
                </h3>
            </div>
            <div class="panel-body">
                <p>
                    <label for="define"><?php echo $Language->get('text_define'); ?> (camelCase)</label>
                    <input
                        type="text"
                        name="define"
                        class="form-control" />
                </p>
                <p>
                    <label for="title"><?php echo $Language->get('text_title'); ?></label>
                    <input
                        type="text"
                        name="title"
                        class="form-control" />
                </p>
                <p>
                    <label for="summary"><?php echo $Language->get('text_summary'); ?></label>
                    <input
                        type="text"
                        name="summary"
                        class="form-control" />
                </p>
                <p>
                    <label for="value"><?php echo $Language->get('text_value'); ?></label>
                    <input
                        type="text"
                        name="value"
                        class="form-control" />
                </p>
                <?php if (Session::get('loggedIn') == true) { ?>
                    <p>
                        <label for="type"><?php echo $Language->get('text_type'); ?></label>
                        <select name="type" class="form-control">
                            <option value="">
                                <?php echo $Language->get('text_select') . ' '  . $Language->get('text_type'); ?>
                            </option>
	                        <?php
	                        foreach (Option::loadAllTypes() as $type) {
		                        ?>
                                <option value="<?php echo $type->getCode(); ?>">
			                        <?php echo $type->getTitle(); ?>
                                </option>
		                        <?php
	                        }
	                        ?>
                        </select>
                    </p>
                    <p>
                        <label for="edit"><?php echo $Language->get('text_edit'); ?></label>
                        <select name="edit" class="form-control">
                            <option value="">
                                <?php echo $Language->get('text_edit'); ?>
                            </option>
                            <option value="1">
                                <?php echo $Language->get('text_yes'); ?>
                            </option>
                            <option value="0">
                                <?php echo $Language->get('text_no'); ?>
                            </option>
                        </select>
                    </p>
                    <p>
                        <label for="system"><?php echo $Language->get('text_system'); ?></label>
                        <select name="system" class="form-control">
                            <option value="">
                                <?php echo $Language->get('text_system'); ?>
                            </option>
                            <option value="1">
                                <?php echo $Language->get('text_yes'); ?>
                            </option>
                            <option value="0">
                                <?php echo $Language->get('text_no'); ?>
                            </option>
                        </select>
                    </p>
                <?php } else { ?>
                    <input type="hidden" name="roles_id" value="3">
                    <input type="hidden" name="status" value="0">
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <button type="submit" class="pull-left btn btn-lg btn-danger">
            <?php echo $Language->get('text_create'); ?>
        </button>
        <button
            type="button"
            class="cancel pull-right btn btn-lg btn-default">
            <?php echo $Language->get('text_cancel'); ?>
        </button>
    </div>
</div>
</form>
