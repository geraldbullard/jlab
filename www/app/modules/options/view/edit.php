<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: edit.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<form method="post" action="/options/editsave/<?php echo $this->options[0]['id']; ?>">
<input type="hidden" name="page" value="<?php echo @$_GET['page']; ?>" />
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $this->options[0]['title']; ?>
                </h3>
            </div>
            <div class="panel-body">
                <p>
                    <label for="define">
                        <?php echo $Language->get('text_define'); ?>
                    </label>
                    <input
                        type="text"
                        name="define"
                        class="form-control"
                        value="<?php echo $this->options[0]['define']; ?>"
                        <?php echo (($this->options[0]['edit'] != 1) ? ' readonly' : ''); ?>
                    />
                </p>
                <p>
                    <label for="title">
                        <?php echo $Language->get('text_title'); ?>
                    </label>
                    <input
                        type="text"
                        name="title"
                        class="form-control"
                        value="<?php echo $this->options[0]['title']; ?>"
                        <?php echo (($this->options[0]['edit'] != 1) ? ' readonly' : ''); ?>
                    />
                </p>
                <p>
                    <label for="summary">
                        <?php echo $Language->get('text_summary'); ?>
                    </label>
                    <input
                        type="text"
                        name="summary"
                        class="form-control"
                        value="<?php echo $this->options[0]['summary']; ?>"
                        <?php echo (($this->options[0]['edit'] != 1) ? ' readonly' : ''); ?>
                    />
                </p>
                <p>
                    <label for="value">
                        <?php echo $Language->get('text_value'); ?>
                    </label>
                    <input
                        type="text"
                        name="value"
                        class="form-control"
                        value="<?php echo $this->options[0]['value']; ?>"
                        <?php echo (($this->options[0]['edit'] != 1) ? ' readonly' : ''); ?>
                    />
                </p>
                <p>
	                <label for="type"><?php echo $Language->get('text_type'); ?></label>
                    <select name="type" class="form-control"
	                    <?php echo (($this->options[0]['edit'] != 1) ? ' readonly' : ''); ?>>
	                    <?php
	                    foreach (Option::loadAllTypes() as $type) {
                        ?>
                        <option value="<?php echo $type->getCode(); ?>" <?php
                            echo ($this->options[0]['type'] == $type->getCode()) ? 'selected' : '';
                        ?>>
                            <?php echo $type->getTitle(); ?>
                        </option>
                        <?php
	                    }
                        ?>
                    </select>
                </p>
                <p>
                    <label for="edit"><?php echo $Language->get('text_edit'); ?></label>
                    <select name="edit" class="form-control"
                        <?php echo (($this->options[0]['edit'] != 1) ? ' readonly' : ''); ?>>
                        <option value="1" <?php echo ($this->options[0]['edit'] == '1') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_yes'); ?>
                        </option>
                        <option value="0" <?php echo ($this->options[0]['edit'] == '0') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_no'); ?>
                        </option>
                    </select>
                </p>
                <p>
                    <label for="system"><?php echo $Language->get('text_system'); ?></label>
                    <select name="system" class="form-control"
                        <?php echo (($this->options[0]['edit'] != 1) ? ' readonly' : ''); ?>>
                        <option value="1" <?php echo ($this->options[0]['system'] == '1') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_yes'); ?>
                        </option>
                        <option value="0" <?php echo ($this->options[0]['system'] == '0') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_no'); ?>
                        </option>
                    </select>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <button type="submit" class="pull-left btn btn-lg btn-danger">
            <?php echo $Language->get('text_save'); ?>
        </button>
        <button
            type="button"
            class="cancel pull-right btn btn-lg btn-default">
            <?php echo $Language->get('text_cancel'); ?>
        </button>
    </div>
</div>
</form>
