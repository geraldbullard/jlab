<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: create.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/roles/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<form method="post" action="/roles/create">
<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $Language->get('text_new') . ' ' . $Language->get('text_role') . ' ' . $Language->get('text_information'); ?>
                </h3>
            </div>
            <div class="panel-body">
                <p>
                    <label for="name"><?php echo $Language->get('text_name'); ?></label>
                    <input
                        type="text"
                        name="name"
                        class="form-control" />
                </p>
                <p>
                    <label for="hidden"><?php echo $Language->get('text_hidden'); ?></label>
                    <select name="hidden" class="form-control">
                        <option value="">
                            <?php echo $Language->get('text_select') . ' ' . $Language->get('text_visibility'); ?>
                        </option>
                        <option value="1">
                            <?php echo $Language->get('text_visible'); ?>
                        </option>
                        <option value="0">
                            <?php echo $Language->get('text_hidden'); ?>
                        </option>
                    </select>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $Language->get('text_module') . ' ' . $Language->get('text_access'); ?>
                </h3>
            </div>
            <div class="panel-body">
                <div class="role-access">
                    <?php
                    foreach (Module::modules() as $module) {
                    ?>
                    <div class="access-row">
                        <label for="access_<?php echo $module['module']; ?>"><?php echo $module['title']; ?></label>
                        <input
                            type="text"
                            name="access[<?php echo $module['module']; ?>]"
                            id="access_<?php echo $module['module']; ?>"
                            value="0"
                            style="display:none;">
                        <div id="access_slider_<?php echo $module['module']; ?>">
                            <div id="custom_handle_<?php echo $module['module']; ?>" class="ui-slider-handle"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <p class="padding-top-10">
            <button type="submit" class="pull-left btn btn-lg btn-danger">
                <?php echo $Language->get('text_save'); ?>
            </button>
            <button
                type="button"
                class="cancel pull-right btn btn-lg btn-default">
                <?php echo $Language->get('text_cancel'); ?>
            </button>
        </p>
    </div>
</div>
</form>