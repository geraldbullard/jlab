<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/roles/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<?php if (Session::getStatusMessage('rolesMessage') != '') { ?>
<div class="row">
    <div class="col-lg-12" id="content-alert-message">
        <div class="alert alert-<?php echo Session::getStatusMessage('rolesMessage')['status']; ?> alert-dismissible alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $Language->get(Session::getStatusMessage('rolesMessage')['value']); ?></h4>
        </div>
    </div>
</div>
<?php Session::setNull('rolesMessage'); } ?>
<div class="row">
    <div class="col-xs-12 col-md-3 role-add-button">
        <a href="/roles/createrole">
            <button class="btn btn-lg btn-primary" type="button">
                <i class="fa fa-plus"></i>
                &nbsp;<?php echo
                    $Language->get('text_add') . ' ' . $Language->get('text_new') .  ' ' . $Language->get('text_role');
                ?>
            </button>
        </a>
    </div>
    <div class="col-xs-12 col-md-9 pull-right" id="roles-filter">
        <div class="box box-info filter-box<?php
        echo (isset($_GET['roles_filter']) && $_GET != '') ? '' : ' collapsed-box';
        ?>">
            <div class="box-header with-border">
                <h3 class="box-title">Search Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-<?php
                        echo (isset($_GET['roles_filter']) && $_GET != '') ? 'minus' : 'plus';
                        ?>"></i></button>
                </div>
            </div>
            <form class="form-horizontal" action="/roles/" method="get">
                <input type="hidden" name="roles_filter" value="1" />
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="roles_search" class="col-sm-3 control-label">Search by Name</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="roles_search"
                                            name="roles_search"
                                            value="<?php
                                            echo (isset($_GET['roles_search']) && $_GET['roles_search'] != ''
                                                ? $_GET['roles_search'] : '');
                                            ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="roles_hidden" class="col-sm-3 control-label">Hidden</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="roles_hidden"
                                            name="roles_hidden"
                                            class="form-control">
                                            <option value="">Select</option>
                                            <option value="1"<?php
                                                echo (isset($_GET['roles_hidden']) && $_GET['roles_hidden'] == 1 ?
                                                    ' selected="selected"' : '')
                                            ?>>Yes</option>
                                            <option value="0"<?php
                                                echo (isset($_GET['roles_hidden']) && $_GET['roles_hidden'] == 0 ?
                                                    ' selected="selected"' : '')
                                            ?>>No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="/roles/"><button type="button" class="btn btn-default">Reset</button></a>
                    <button type="submit" class="btn btn-info pull-right">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body no-padding">
                <table class="table">
                    <tr>
                        <th><?php echo $Language->get('text_id'); ?></th>
                        <th><?php echo $Language->get('text_name'); ?></th>
                        <th class="center"><?php echo $Language->get('text_hidden'); ?></th>
                        <th class="text-right"><?php echo $Language->get('text_actions'); ?></th>
                    </tr>
                    <?php
                    foreach ($this->rolesList as $key => $value) {
                        echo '<tr>';
                        echo '  <input type="hidden" name="roles_id" value="' . $value['id'] . '">';
                        echo '  <td>' . $value['id'] . '</td>';
                        echo '  <td>' . $value['name'] . '</td>';
                        echo '  <td class="center">' . ($value['hidden'] == 1 ? $Language->get('text_yes') : $Language->get('text_no')) . '</td>';
                        echo '  <td><div class="pull-right btn-group">';
                        if (Session::get('roles_access') >= ACCESS_EDIT) {
                            echo '    <button
                                        class="btn btn-xs btn-primary"
                                        type="button"
                                        onclick="location.href=\'/roles/edit/' . $value['id'] . '\'">
                                        <i class="fa fa-pencil"></i> <span class="hide-below-480">' . $Language->get('text_edit') . '</span>
                                      </button>';
                        }
                        if (Session::get('roles_access') >= ACCESS_DELETE) {
                            echo '    <button
                                        class="delete-role btn btn-xs btn-danger"
                                        type="button" data-role-id="' . $value['id'] . '">
                                        <i class="fa fa-trash-o"></i> <span class="hide-below-480">' . $Language->get('text_delete') . '</span>
                                      </button>';
                        }
                        echo '  </div></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="displaying-div">
            <?php echo Utils::returnDisplayingRowInfo($this->rpp, $this->page, $this->rolesListResults); ?>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="pagination-div">
            <?php echo Utils::returnPagination(
                @$this->params, $this->totalPages, $this->page, false, $this->rolesListResults
            ); ?>
        </div>
    </div>
</div>
