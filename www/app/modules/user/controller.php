<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class User extends Controller {

    public function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /noaccess/');
            exit;
        }
    }
    
    public function index() {
        global $Options;
        // check for view permissions
        if (Session::get('user_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // added for pagination
            global $Options;
            $this->view->rpp = $Options['core']['usersAdminPagination'];
            if (@$_GET['page']) $page = intval(@$_GET['page']);
            if (@$page <= 0) $page = 1;
            $this->view->page = $page;
            $this->view->limit = (($page-1) * $this->view->rpp) . ',' . $this->view->rpp;
            if (isset($_GET['user_filter']) && $_GET['user_filter'] != '') {
                $this->view->params = array(
                    'user_filter' => 1,
                    'user_search' => $_GET['user_search'],
                    'user_role' => $_GET['user_role'],
                    'user_status' => $_GET['user_status'],
                );
            }
            if (isset($_GET['user_filter']) && $_GET['user_filter'] != '') {
                $this->view->userList = $this->model->userList($this->view->params, $this->view->limit);
                $this->view->userListResults = count($this->model->userList($this->view->params));
            } else {
                $this->view->userList = $this->model->userList('', $this->view->limit);
                $this->view->userListResults = count($this->model->userList());
            }
            $this->view->totalPages = ceil($this->view->userListResults/$this->view->rpp);
            // START Search Filter
            $getRoles = $this->model->getRoles();
            foreach ($getRoles as $i => $role) {
                $rolesArr[] = array(
                    'id' => $role['id'],
                    'name' => $role['name'],
                    'hidden' => $role['hidden'],
                );
            }
            $this->view->userRolesArr = $rolesArr;
            $this->view->getTheme('admin');
            $this->view->heading = 'Users';
            $this->view->renderHeader();
            $this->view->renderView('user/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('user/view/js/index.js.php');
            $this->view->renderBottom();
        }
    }
    
    public function createUser() {
        // check for create permissions
        if (Session::get('user_access') < ACCESS_CREATE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // continue to create user page if enough permission
            $this->view->getTheme('admin');
            $this->view->heading = 'Create New User';
            $this->view->renderHeader();
            $this->view->renderView('user/view/create');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('user/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }
    
    public function create() {
        // check for create permissions
        if (Session::get('user_access') < ACCESS_CREATE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // create user if enough permission
            $postData = array();
            $postData['firstname'] = $_POST['firstname'];
            $postData['lastname'] = $_POST['lastname'];
            $postData['email'] = $_POST['email'];
            $postData['username'] = $_POST['username'];
            $postData['password'] = $_POST['password'];
            $postData['roles_id'] = ($_POST['roles_id'] != '' ? $_POST['roles_id'] : '3');
            $postData['status'] = ($_POST['status'] != '' ? $_POST['status'] : '0');
            $postData['gender'] = $_POST['gender'];

            // @TODO: Do your error checking!

            if ($this->model->create($postData)) {
                header('Location: /user?msg=create_success');
                exit;
            } else {
                header('Location: /user/createuser/?msg=create_failed');
                exit;
            }
            exit;
        }
    }
    
    public function edit($id = '') {
        // check for edit permissions
        if (Session::get('user_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // check if id is empty
            if (empty($id)) {
                // redirect if id is empty
                header('Location: /user/');
                exit;
            }
            // continue to user edit page if enough permission
            $this->view->user = $this->model->userSingleList($id);
            $this->view->getTheme('admin');
            $this->view->heading = 'Edit User';
            $this->view->renderHeader();
            $this->view->renderView('user/view/edit');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('user/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }
    
    public function editSave($id = '') {
        // check for edit permissions
        if (Session::get('user_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // save user if enough permission
	        $postData = array();
	        $postData['id'] = $id;
	        $postData['firstname'] = $_POST['firstname'];
	        $postData['lastname'] = $_POST['lastname'];
	        $postData['email'] = $_POST['email'];
	        $postData['username'] = $_POST['username'];
	        $postData['password'] = $_POST['password'];
	        $postData['roles_id'] = $_POST['roles_id'];
	        $postData['status'] = $_POST['status'];
	        $postData['gender'] = $_POST['gender'];

            // @TODO: Do your error checking!

	        $this->model->editsave($postData);
            header(
                'location: /user/' . (
                isset($_POST['page']) && $_POST['page'] != '' ? '?page=' . $_POST['page'] : ''
                )
            );
            exit;
        }
    }
    
    public function delete($id = '') {
        // check for delete permissions
        if (Session::get('user_access') < ACCESS_DELETE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // delete user if enough permission
            $this->model->delete($id);
            header('location: /user/');
            exit;
        }
    }

}
