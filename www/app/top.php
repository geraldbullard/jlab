<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: top.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

    error_reporting(E_ALL);

    // Always provide a TRAILING SLASH (/) AFTER A PATH
    define('APP', 'app/');
    define('MODULES', __DIR__ . '/modules/');
    define('LIBS', 'libs/');

    // The sitewide hashkey, do not change this because its used for passwords!
    // This is for other hash keys... Not sure yet
    define('HASH_GENERAL_KEY', 'gn5pl4n3t');

    // This is for database passwords only
    define('HASH_PASSWORD_KEY', 'B0ngW4t3RCr0ss3y3Dn0b1tch3s');

    // set the permission levels
    define('ACCESS_NONE', '0');
    define('ACCESS_VIEW', '1');
    define('ACCESS_EDIT', '2');
    define('ACCESS_CREATE', '3');
    define('ACCESS_DELETE', '4');
    define('ACCESS_GLOBAL', '5');

    require(__DIR__ . '/db.php');
	require(LIBS . 'Core.php');

	spl_autoload_register(function($class) {
		include(LIBS . $class . '.php');
	});

    // Load the Core
    $core = new Core();

    // Optional Path Settings
    // $core->setControllerPath();
    // $core->setModelPath();
    // $core->setDefaultFile();
    // $core->setErrorFile();
    $core->init();

	// Set the installed modules array
	foreach ($Modules as $key => $module) {
		$installed_modules[] = $key;
	}

    // Look for any uninstalled modules
    $module_folders = array_diff(scandir(MODULES), array('..', '.'));
    foreach ($module_folders as $module) {
        if (!in_array($module, $installed_modules)) {
            if (is_file(MODULES . $module . '/init.php')) {
                include(MODULES . $module . '/init.php');
            }
        }
    }

    /*
    additional module work possible use
    [admin] =>
    [id] => 1
    [module] => admin
    [title] => Dashboard
    [summary] => The Admin...
    [theme] => admin
    [icon] => fa-dashboard
    [sort] => 0
    [visibility] => 1
    [status] => 1
    [system] => 1
    if (is_file(MODULES . $module . '/class.php')) {
        include(MODULES . $module . '/class.php');
    }
    */

    // Rci extend application top
    echo $Rci->get('apptop', 'bottom', false);
