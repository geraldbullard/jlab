<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: Content.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class ContentTypes
{
    protected static $data;

    const CONTENT_TYPE_PAGE = 1;
    const CONTENT_TYPE_POST = 2;
    const CONTENT_TYPE_CATEGORY = 3;

    public static function loadAll()
    {
        if (!isset(self::$data)) {
            self::lazyLoadTypes();
        }

        return self::$data;
    }

    public static function loadById($id)
    {
        if (!isset(self::$data)) {
            self::lazyLoadTypes();
        }

        for ($i = 0; $i < count(self::$data); $i++) {
            if (self::$data[$i]->getId() == $id) {
                return self::$data[$i];
            }
        }

        return false;
    }

    protected static function lazyLoadTypes()
    {
        self::$data = array
        (
            new ContentType(array
            (
                'id' => self::CONTENT_TYPE_PAGE,
                'title' => 'Page',
                'type' => 'page',
                'icon' => 'fa-file-text-o',
            )),
            new ContentType(array
            (
                'id' => self::CONTENT_TYPE_POST,
                'title' => 'Post',
                'type' => 'post',
                'icon' => 'fa-thumb-tack',
            )),
            new ContentType(array
            (
                'id' => self::CONTENT_TYPE_CATEGORY,
                'title' => 'Category',
                'type' => 'category',
                'icon' => 'fa-folder-o',
            ))
        );
    }

}

class ContentType
{
    protected $id;
    protected $title;
    protected $type;
    protected $icon;

    public function __construct($properties)
    {
        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{"$property"} = $value;
            }
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

}
