<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: Contents.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Contents
{

    /* Private variables */
    var $_contents = array();

    /* Class constructor */
    public static function getContentsCount()
    {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $count = $conn->select(
            "SELECT COUNT(*) AS `count` 
            FROM " . DB_PREFIX . "content;"
        );

        return $count[0]['count'];
    }

}
