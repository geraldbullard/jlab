<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: Core.php, v1.0 2015-08-19 maestro Exp $
 * @location /libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Core
{

    private $url = null;
    private $controller = null;
    private $controllerPath = 'app/modules/'; // Always include trailing slash
    private $modelPath = 'app/modules/'; // Always include trailing slash
    private $errorFile = 'error/controller.php';
    private $defaultFile = 'index/controller.php';

    /**
     * Starts the Core
     *
     * @return boolean
     */
    public function init()
    {
        // set the authorization (logged in status)
        require(LIBS . 'Auth.php');
        $logged = '';
        Auth::handleLogin();

        // Sets the protected $url
        $this->getUrl();

        // working on permalink type urls
        foreach ($this->url as $part) {
            if (stristr($part, ".html")) {
                $this->url[0] = $part;
            }
        }
        // set the current view
        $this_url = str_replace(".html", "", $this->url[0]);
        $_SESSION['current_view'] = $this_url;
        if (empty($_SESSION['current_view'])) {
            $_SESSION['current_view'] = 'index';
        }

        // set the config values for site usage
        global $Options;
        require LIBS . 'Option.php';
        $Option = new Option();
        $Options = $Option->_options;

        // set the correct timezone based on the Default TimeZone in options
        date_default_timezone_set($Options['core']['defaultTimeZone']);

        // set the language related data
        global $Language;
        require LIBS . 'Language.php';
        $Language = new Language();
        $Language->load('core');
        global $Definitions;
        $Definitions = new Language();
        $Definitions->load($_SESSION['current_view']);
        $Definitions = $Definitions->_definitions;

        // set the module related data
        global $Modules;
        require LIBS . 'Module.php';
        $Module = new Module();
        $Modules = $Module->_modules;

        // set the content related data (types)
        global $ContentTypes;
        require LIBS . 'ContentTypes.php';
        $ContentTypes = new ContentTypes();
        $ContentTypes = $ContentTypes->loadAll();
        
        // set the menu related data
        global $Menu;
        require LIBS . 'Menu.php';
        $Menu = new Menu();
        $Menu = $Menu->_items;

        // set the rci data
        global $Rci;
        require LIBS . 'Rci.php';
        $Rci = new Rci();

        // Load the default controller if no URL is set
        // eg: Visit http://localhost it loads Default Controller
        if (empty($this_url)) {
            $this->loadDefaultController();
            return false;
        }

        $this->loadExistingController();
        $this->callControllerMethod();
    }

    /**
     *
     * (Optional) Set a custom path to controllers
     * @param string $path
     *
     */
    public function setControllerPath($path)
    {
        $this->controllerPath = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to models
     * @param string $path
     */
    public function setModelPath($path)
    {
        $this->modelPath = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to the error file
     * @param string $path Use the file name of your controller, eg: error.php
     */
    public function setErrorFile($path)
    {
        $this->errorFile = trim($path, '/');
    }

    /**
     * (Optional) Set a custom path to the error file
     * @param string $path Use the file name of your controller, eg: index.php
     */
    public function setDefaultFile($path)
    {
        $this->defaultFile = trim($path, '/');
    }

    /**
     * Fetches the $_GET from 'url'
     */
    private function getUrl()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->url = explode('/', $url);
    }

    /**
     * This loads if there is no GET parameter passed
     */
    private function loadDefaultController()
    {
        require $this->controllerPath . $this->defaultFile;
        $this->controller = new Index();
        $this->controller->index();
    }

    /**
     * Load an existing controller if there IS a GET parameter passed
     *
     * @return boolean|string
     */
    private function loadExistingController()
    {
        // working on permalink type urls
        foreach ($this->url as $part) {
            if (stristr($part, ".html")) {
                $this->url[0] = $part;
            }
        }

        $this_url = str_replace(".html", "", $this->url[0]);
        $file = $this->controllerPath . $this_url . '/controller.php';

        if (file_exists($file)) {
            require $file;
            $this->controller = new $this_url;
            $this->controller->loadModel($this_url, $this->modelPath);
        } else {
            $slug_exists = true;
            if ($slug_exists == true) {
                require 'app/modules/index/controller.php';
                $this->controller = new index;
                $this->controller->loadModel('index', 'app/modules/');
            } else {
                $this->error();
                return false;
            }
        }
    }

    /**
     * If a method is passed in the GET url paremter
     *
     *  http://localhost/controller/method/(param)/(param)/(param)
     *  url[0] = Controller
     *  url[1] = Method
     *  url[2] = Param
     *  url[3] = Param
     *  url[4] = Param
     */
    private function callControllerMethod()
    {
        $length = count($this->url);
        foreach ($this->url as $url_part) {
            if (stristr($url_part, ".html")) {
                $length = 1;
                break;
            }
        }

        // Make sure the method we are calling exists
        /*if ($length > 1) {
            if (!method_exists($this->controller, $this->url[1])) {
                $this->error();
            }
        }*/

        // Determine what to load
        switch ($length) {
            case 5:
                // Controller->Method(Param1, Param2, Param3, Param4)
                $this->controller->{$this->url[1]}($this->url[2], $this->url[3], $this->url[4]);
                break;

            case 4:
                // Controller->Method(Param1, Param2, Param3)
                $this->controller->{$this->url[1]}($this->url[2], $this->url[3]);
                break;

            case 3:
                // Controller->Method(Param1, Param2)
                $this->controller->{$this->url[1]}($this->url[2]);
                break;

            case 2:
                // Controller->Method(Param1)
                $this->controller->{$this->url[1]}();
                break;

            default:
                $this->controller->index();
                break;
        }
    }

    /**
     * Display an error page if nothing exists
     *
     * @return boolean
     */
    private function error()
    {
        require $this->controllerPath . $this->errorFile;
        $this->controller = new Error();
        $this->controller->index();
        exit;
    }

}
