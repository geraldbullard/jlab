<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: View.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class View
{

    public function __construct()
    {
        // get theme from db later
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
    }

    public function getContent()
    {
        // working on permalink type urls
        if (stristr($_GET['url'], "/")) {
            $_GET['url'] = end(explode("/", $_GET['url']));
        }

        if (isset($_GET['url']) && strpos($_GET['url'], ".html")) {
            $content = $this->db->select(
                "SELECT *
                FROM " . DB_PREFIX . "content
                WHERE slug = '" . str_replace(".html", "", $_GET['url']) . "'
                LIMIT 1"
            );
            return $content[0];
        } else {
            /* In case it's not an index module based page for the front end like Login or Logout
            array(
                'type' => $data['type'],
                'title' => $data['title'],
                'slug' => $data['slug'],
                'summary' => $data['summary'],
                'content' => $data['content'],
                'menuTitle' => $data['menuTitle'],
                'override' => $data['override'],
                'metaDescription' => $data['metaDescription'],
                'metaKeywords' => $data['metaKeywords'],
                'sort' => $data['sort'],
                'status' => $data['status'],
                'siteIndex' => $data['siteIndex'],
                'botAction' => $data['botAction'],
                'menu' => $data['menu'],
                'categoryId' => $data['categoryId'],
                'lastModified' => date("Y-m-d")
            )*/
            if (strpos($_GET['url'], "login") !== false) return array('title' => 'Login');

        }
    }

    public function getTheme($module = '')
    {
        $Theme = $this->db->selectCol(
            "SELECT value
            FROM " . DB_PREFIX . "options
            WHERE define = '" . $module . "Theme'
            LIMIT 1"
        );
        $this->theme = $Theme;
    }

    public function renderHeader()
    {
        require 'public/themes/' . $this->theme . '/header.php';
    }

    public function renderAdminBreadCrumb($breadcrumb_array = '')
    {
        global $Language;
        $bcstring = '<div class="admin-breadcrumb">';
        $bcstring .= $Language->get('text_home');
        foreach ($breadcrumb_array as $breadcrumb) {
            if ($breadcrumb['view'] != 'index') {
                $bcstring .= ' &raquo; <a href="' . $breadcrumb['view'] . '">' . $breadcrumb['name'] . '</a>';
            }
        }
        $bcstring .= '</div>';
        echo $bcstring;
    }

    public function renderView($name = '', $noInclude = false)
    {
        require 'app/modules/' . $name . '.php';
    }

    public function renderFooter()
    {
        require 'public/themes/' . $this->theme . '/footer.php';
    }

    public function renderScripts()
    {
        require 'public/themes/' . $this->theme . '/scripts.php';
    }

    public function setHeaderCssInclude($module = '', $file = '')
    {
    	Session::setHeaderCssInclude($module, $file);
    }

    public function setFooterJsInclude($module = '', $file = '')
    {
    	Session::setFooterJsInclude($module, $file);
    }

    public function renderModuleJs($file = '')
    {
        require 'app/modules/' . $file;
    }

    public function renderBottom()
    {
        require 'public/themes/' . $this->theme . '/bottom.php';
    }

}
