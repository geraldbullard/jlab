/** USEFUL FRONTEND QUERY FUNCTIONALITY
 * Last updated by Daniel Bage
 * On 03/15/2017
 *
 * Last method added was swapChildElements
 *
 * METHODS
 * viewportLoaded
 * documentReady
 * viewportOrientationChanged
 * viewportResized
 * viewportResizedFinished
 * viewportWidth
 * matchHeights
 * tallestHeight
 * restoreHeights
 * matchHeightsOfEqualVerticalPosition
 * matchHeightsOfSecondElement
 * swapElements
 * isDevice
 * fadeIn
 * getQueryVariable
 * addOrRemoveStackClass
 * stackCells
 * elementDistanceFromViewportrtbs
 * fixHeader
 * fixFooter
 * freeFooter
 * clearContentFromHeader
 * clearContentFromFooter
 * maximiseContentArea
 * externalLinksTargetBlank
 * addHvrClass
 * addClassToLiWithPdfs
 * addClassToLiWithExternalLinks
 * removeEmptyElements
 * print_r
 * documentScrolled
 * swapChildElements
 *
 */


var Utils =
    {
        /**
         * Enum for device names.
         * @readonly
         * @enum {string}
         */
        device: {
            CELL: 'CELL', // < 380
            SMALL_TABLET: 'SMALL TABLET', // 380 - 480
            TABLET: 'TABLET', // 480 - 768
            SMALL_DESKTOP: 'SMALL DESKTOP', // 768 - 1024
            DESKTOP: 'DESKTOP', // 1024 - 1199
            LARGE_DESKTOP: 'LARGE DESKTOP' // > 1200
        },
        /**
         * @description Once viewport is loaded, perform callback
         * @param {function} [callback] Callback for window load
         */
        viewportLoaded: function (callback) {
            jQuery(window).load(callback);
        },
        /**
         * @description Once document is ready, perform callback
         * @param {function} [callback] Callback for document ready
         */
        documentReady: function (callback) {
            jQuery(document).ready(callback);
        },
        /**
         * @description Once orientation is changed, perform callback
         * @param {function} [callback] Callback for orientation changed
         */
        viewportOrientationChanged: function (callback) {
            window.addEventListener("orientationchange", callback, false);
        },
        /**
         * @description As viewport is re-sized, perform callback
         * @param {function} [callback] Callback for viewport resizing
         */
        viewportResized: function (callback) {
            jQuery(window).resize(callback);
        },
        /**
         * @description Once viewport resizing ends, perform callback
         * @param {function} [callback] Callback for viewport resizing ending
         */
        viewportResizedFinished: function (callback) {
            var resizeTimer;
            jQuery(window).on('resize', function (e) {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                    callback();
                }, 250);
            });
        },
        /**
         * @description Pixel-less viewport width
         * @returns {number} Viewport width
         */
        viewportWidth: function () {
            var viewportWidth = jQuery(window).width();
            return viewportWidth;
        },
        /**
         * @description Set heights equally on source elements
         * @param {string|JQuery} source The source elements to match heights of
         * @param {boolean} [includeMargin = false] Whether to include element margins
         */
        matchHeights: function (source, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }

            for (var i = 0; i < list.length; i++) {
                var src = jQuery(list[i]);
                var tgtHt = 0;
                src.each(function () {
                    if ((jQuery(this).outerHeight(includeMargin)) > tgtHt) {
                        tgtHt = jQuery(this).outerHeight(includeMargin);

                    }
                });
                jQuery(src).css('height', (tgtHt));
            }
        },
        /**
         * @description Return pixel-less value of tallest source element
         * @param {string|JQuery} source The source elements to compare heights of
         * @param {boolean} [includeMargin = false] Whether to include element margins
         * @returns {number} Pixel-less height
         */
        tallestHeight: function (source, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }

            for (var i = 0; i < list.length; i++) {
                var src = jQuery(list[i]);
                var tgtHt = 0;
                src.each(function () {
                    if ((jQuery(this).outerHeight(includeMargin)) > tgtHt) {
                        tgtHt = jQuery(this).outerHeight(includeMargin);

                    }
                });
                return tgtHt;
            }
        },
        /**
         * @description Remove all css heights from source elements
         * @param {string|JQuery} source The source elements to remove css heights of
         */
        restoreHeights: function (source, callback) {
            var list = [];
            var resizeTimer;
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }
            for (var i = 0; i < list.length; i++) {
                jQuery(list[i]).css("height", "");
            }
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function () {
                callback;
            }, 250);
        },
        /**
         * @description Set heights equally on source elements with same vertical position
         * @param {string|JQuery} source The source elements to match
         * @param {boolean} [includeMargin = false] Whether to include element margins
         */
        matchHeightsOfEqualVerticalPosition: function (source, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }
            for (var i = 0; i < list.length; i++) {
                var src = jQuery(list[i]);

                var elementsWithEqualVerticalPositions = [];
                var positionOfFirstElement = jQuery(src[0]).position();
                if (source == '.cta_box') {
                }
                var verticalPositionOfFirstElement = 0;
                if (positionOfFirstElement) {
                    verticalPositionOfFirstElement = positionOfFirstElement.top;
                }
                var y = 0;
                jQuery.each(src, function () {
                    var srcP = jQuery(this).position();
                    var srcY = srcP.top;
                    if (srcY == y) {
                        jQuery(this).addClass('match');
                    }
                    else {
                        Utils.matchHeights('.match', includeMargin);
                        srcP = jQuery(this).position();
                        srcY = srcP.top;
                        y = srcY;
                        jQuery('.match').removeClass('match');
                        jQuery(this).addClass('match');
                    }
                });

                Utils.matchHeights('.match', includeMargin);
                jQuery('.match').removeClass('match');
            }
        },
        /**
         * @description Make height of element A match element B
         * @param {string} elementA The element to receive new height
         * @param {string} elementB The element to match height of
         * @param {boolean} [includeMargin = false] Whether to include the elementA margin
         */
        matchHeightsOfSecondElement: function (elementA, elementB, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var heightOfSecondSource = jQuery(elementB).outerHeight(includeMargin);
            jQuery(elementA).css('height', heightOfSecondSource);
        },
        /**
         * @description Move element B infront of element A
         * @param {string} elemA First Element
         * @param {string} elemB Second element to be moved in front of first
         */
        swapElements: function (elemA, elemB) {
            jQuery(elemA).before(jQuery(elemB));
        },
        /**
         * @description Determine device based on viewport width
         * @param {Utils.device} deviceName Device name
         * @returns {boolean}
         */
        isDevice: function (deviceName) {
            deviceName = deviceName.toUpperCase();
            var width = jQuery(window).width();
            if ((width < 380) && deviceName == Utils.device.CELL) {
                return true
            } else if ((width < 480) && deviceName == Utils.device.SMALL_TABLET) {
                return true
            } else if ((width < 768) && deviceName == Utils.device.TABLET) {
                return true
            } else if ((width < 1024) && deviceName == Utils.device.SMALL_DESKTOP) {
                return true
            } else if ((width < 1199) && deviceName == Utils.device.DESKTOP) {
                return true
            } else if ((width > 1199) && deviceName == Utils.device.LARGE_DESKTOP) {
                return true
            } else {
                return false
            }
        },
        /**
         * @description Once viewport finishes loading, fade in source element
         * @param {string|JQuery} source Source element to fade in
         * @param {number} [delay = 0] Delay before fade time in milliseconds
         * @param {number} [fadeInTime = 0] Fade duration in milliseconds
         */
        fadeIn: function (source, delay, fadeInTime) {
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }
            for (var i = 0; i < list.length; i++) {
                jQuery(list[i]).each(function (index) {
                    var item = jQuery(this);
                    item.css('opacity', 0);
                    Utils.viewportLoaded(function () {
                        item.delay(delay * index).animate({
                            opacity: 1
                        }, fadeInTime);
                    });
                });
            }
        },
        getQueryVariable: function (variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return (false);
        },
        addOrRemoveStackClass: function (commaSeperatedStringOfSelectors) {
            var cellsToStack = jQuery(commaSeperatedStringOfSelectors);
            cellsToStack.each(function () {
                if (Utils.isDevice('CELL')) {
                    jQuery(this).addClass('stack');
                } else {
                    jQuery(this).removeClass('stack');
                }
            })
        },
        stackCells: function (commaSeperatedStringOfSelectors) {
            if (typeof commaSeperatedStringOfSelectors === 'undefined') {
                commaSeperatedStringOfSelectors = '.cell';
            }
            Utils.documentReady(function () {
                Utils.addOrRemoveStackClass(commaSeperatedStringOfSelectors);
            });
            Utils.viewportOrientationChanged(function () {
                Utils.addOrRemoveStackClass(commaSeperatedStringOfSelectors);
            });
            Utils.viewportResized(function () {
                Utils.addOrRemoveStackClass(commaSeperatedStringOfSelectors);
            });
        },
        elementDistanceFromViewport: function (elem, side) {
            var scroll = jQuery(window).scrollTop();
            var elementOffset = jQuery(elem).parent().offset().top;
            var windowHeight = jQuery(window).height();
            var distance = '';

            switch (side) {
                case 'right':

                    break;
                case 'bottom':
                    elementOffset = (jQuery(elem).offset().top);
                    distance = Math.abs((windowHeight - elementOffset) - scroll);
                    break;
                case 'left':

                    break;
                default:
                    distance = (elementOffset - scroll);
            }
            return distance
        },
        /**
         * @description Fix the header to the top of the viewport
         * @param {string} [header_elem = '#headerwrap']
         */
        fixHeader: function (header_elem) {
            //defaults
            var header = jQuery('#headerwrap');

            //overwrite if provided
            if (typeof header_elem !== 'undefined') {
                header = jQuery(header_elem);
            }

            //set top based on logged in or not
            var top = jQuery('#wpadminbar').outerHeight();

            //add css
            header.css({
                'position': 'fixed',
                'top': top,
                'width': '100%',
                'z-index': '3000',
                'box-shadow': '0 0 10px #000000'
            });

            //clear content from header
            this.clearContentFromHeader();
        }
        ,
        /**
         * @description Fix the footer to the bottom of the viewport
         * @param {string} [footer_elem = '#footerwrap']
         */
        fixFooter: function (footer_elem) {
            //defaults
            var footer = jQuery('#footerwrap');

            //overwrite if provided
            if (typeof footer_elem !== 'undefined') {
                footer = jQuery(footer_elem);
            }

            //add css
            footer.css({
                'position': 'fixed',
                'bottom': '0',
                'width': '100%',
                'z-index': '1'
            });

            //clear content from footer
            this.clearContentFromFooter();
        },
        /**
         * @description Remove footer css styling
         * @param {string} [footer_elem = '#footerwrap'] Footer area element
         */
        freeFooter: function (footer_elem) {
            //defaults
            var footer = jQuery('#footerwrap');

            //overwrite if provided
            if (typeof footer_elem !== 'undefined') {
                footer = jQuery(footer_elem);
            }

            //remove css
            footer.removeAttr('style');
        },
        /**
         * @description Add padding to top of content area, equal to header height
         * @param {string} [headerElem = '#headerwrap'] Header area element
         * @param {string} [contentElem = '#contentwrap'] Content area element
         */
        clearContentFromHeader: function (headerElem, contentElem) {
            //defaults
            var header = jQuery('#headerwrap');
            var content = jQuery('#middlewrap');

            //overwrite if provided
            if (typeof headerElem !== 'undefined') {
                header = jQuery(headerElem);
            }
            if (typeof contentElem !== 'undefined') {
                content = jQuery(contentElem);
            }

            //add css
            content.css('padding-top', header.outerHeight());
        },
        /**
         * @description Add padding to bottom of content area, equal to footer height
         * @param {string} [contentElem = '#middlewrap'] Content area element
         * @param {string} [footerElem = '#footerwrap'] Footer area element
         */
        clearContentFromFooter: function (contentElem, footerElem) {
            //defaults
            var content = jQuery('#middlewrap');
            var footer = jQuery('#footerwrap');

            //overwrite if provided
            if (typeof contentElem !== 'undefined') {
                content = jQuery(contentElem);
            }
            if (typeof footerElem !== 'undefined') {
                footer = jQuery(footerElem);
            }

            //add css
            content.css('padding-bottom', footer.outerHeight());
        },
        /**
         * @description Maximise content area so it at least fills the viewport
         * @param {string} [headerElem = '#header'] Header element
         * @param {string} [contentElem = '#middlewrap'] Content element
         * @param {string} [footerElem = '#footerwrap'] Footer element
         */
        maximiseContentArea: function (headerElem, contentElem, footerElem) {
            //defaults
            var header = jQuery('#headerwrap');
            var content = jQuery('#middlewrap');
            var footer = jQuery('#footerwrap');
            var adminBar = jQuery('#wpadminbar');

            //overwrite if provided
            if (typeof headerElem !== 'undefined') {
                header = jQuery(headerElem);
            }
            if (typeof contentElem !== 'undefined') {
                content = jQuery(contentElem);
            }
            if (typeof footerElem !== 'undefined') {
                footer = jQuery(footerElem);
            }

            //additional vars
            var windowHeight = jQuery(window).height();
            var headerHeight = header.outerHeight();
            var footerHeight = footer.outerHeight();
            var adminBarHeight = adminBar.outerHeight();

            if ((header).css('position') == 'fixed') {
                headerHeight = 0;
            }

            if ((footer).css('position') == 'fixed') {
                footerHeight = 0;
            }

            //add css
            content.css('min-height', windowHeight - headerHeight - footerHeight - adminBarHeight);
        },
        /**
         * @description Adds target="_blank" to all links that leave the current domain
         */
        externalLinksTargetBlank: function () {
            //get the domain
            var domain = location.hostname + (location.port ? ':' + location.port : '');
            var setTarget = true;
            var target = null;

            //loop through each link element that doesn't already have a target
            jQuery('a:not([target])').each(function (index, element) {
                setTarget = false;
                target = null;

                //if the link is javascript, don't mess with it
                if (element.href.indexOf('javascript:') != -1) {
                    // break out, go to next element
                    return true;
                }

                // href doesn't contain our domain
                if (element.href.indexOf(domain) == -1) {
                    target = "_blank";
                    setTarget = true;
                }

                // link opens a pdf, Word doc, csv, or txt file
                if (jQuery.inArray(
                        element.href.substr(element.href.lastIndexOf('.') + 1),
                        ['pdf', 'doc', 'docx', 'csv', 'txt']) > -1) {
                    target = "_blank";
                    setTarget = true;
                }

                // set the element's target
                if (setTarget !== false) {
                    element.target = target;
                    jQuery(element).addClass('external_link');
                }

                return true;
            });
            this.addClassToLiWithExternalLinks();
        },
        addHvrClass: function (hoverType, elemString) {
            /*hoverTypes:
             grow, shrink,
             pulse, pulse-grow, pulse-shrink,
             push, pop,
             bounce-in, bounce-out,
             rotate, grow-rotate,
             float, sink, bob, hang,
             skew, skew-forward, slew-backward,
             wobble-horizontal, wobble-vertical, wobble-to-bottom-right, wobble-to-top-right, wobble-top, wobble-bottom, wobble-skew
             buzz, buzz-out

             see http://ianlunn.github.io/Hover/ for more
             */
            jQuery(elemString).each(function () {
                jQuery(this).addClass('hvr-' + hoverType).css('cursor', 'pointer');
            });
        },
        /**
         * @description Add .pdf to li elements that contain links to PDFS
         */
        addClassToLiWithPdfs: function () {
            jQuery('[href$= ".pdf"]').each(function () {
                jQuery(this).parent('li').addClass('contains_pdf');
            });
        },
        /**
         * @description Add .external_link to li elements that contain links to external web pages
         */
        addClassToLiWithExternalLinks: function () {
            jQuery('.external_link').each(function () {
                jQuery(this).parent('li').addClass('contains_external_link');
            });
        },
        /**
         * @description Remove empty elements
         * @param {string} [element = p] The element type to remove.
         */
        removeEmptyElements: function (element) {
            if (!element) {
                element = 'p';
            }
            jQuery(element).each(function () {
                var elem = jQuery(this);
                if (elem.html().replace(/\s|&nbsp;/g, '').length == 0)
                    elem.remove();
            });
        },
        /**
         * @description PHP-like print_r() & var_dump() equivalent for JavaScript Object
         * @author Faisalman - movedpixel@gmail.com
         * @license http://www.opensource.org/licenses/mit-license.php
         * @link http://gist.github.com/879208
         *
         * examples:
         *   // alert a boolean value
         *   alert(Utils.isDevice("TABLET"));
         *   // alert an object/array
         *   alert(Utils.print_r(Utils.device));
         */
        print_r: function (obj) {
            // define tab spacing
            var tab = '';
            // check if it's array
            var isArr = Object.prototype.toString.call(obj) === '[object Array]' ? true : false;
            // use {} for object, [] for array
            var str = isArr ? ('Array [' + tab + '\n') : ('Object {' + tab + '\n');
            // walk through it's properties
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    var val1 = obj[prop];
                    var val2 = '';
                    var type = Object.prototype.toString.call(val1);
                    switch (type) {
                        // recursive if object/array
                        case '[object Array]':
                        case '[object Object]':
                            val2 = print_r(val1, (tab + '\t'));
                            break;
                        case '[object String]':
                            val2 = '\'' + val1 + '\'';
                            break;
                        default:
                            val2 = val1;
                    }
                    str += tab + '\t' + prop + ' => ' + val2 + ',\n';
                }
            }
            // remove extra comma for last property
            str = str.substring(0, str.length - 2) + '\n' + tab;
            return isArr ? (str + ']') : (str + '}');
            var var_dump = print_r; // equivalent function
        },
        /**
         * @description Add 'scrolled' class to body when an element is a specified distance from the top of the viewport
         * @param {int} [triggerDistance = -100] The distance the element is from the viewport top.
         * @param {string} [triggerElement = #middlewrap] The element that acts as a trigger.
         */
        documentScrolled: function (triggerDistance, triggerElement) {
            if (!triggerDistance) {
                triggerDistance = -65;
            }
            if (!triggerElement) {
                triggerElement = '#main';
            }
            // adding scrolled class
            var headerClear = true;
            var body = jQuery('body');
            var timeout;

            jQuery(window).scroll(function () {
                    clearTimeout(timeout);

                    var headerY = jQuery(triggerElement).offset().top;
                    var scroll = headerY - jQuery(window).scrollTop();
                    if ((scroll < triggerDistance) && headerClear) {
                        body.toggleClass('scrolled');
                        headerClear = false;

                    }
                    else if ((scroll > triggerDistance) && !headerClear) {
                        body.toggleClass('scrolled');
                        headerClear = true;

                    }
                    timeout = setTimeout(function () {
                        // do your stuff
                    }, 500);
                }
            )
        },
        /**
         * @description Move element B infront of element A inside many children elements
         * @param {string} childElem Child Element
         * @param {string} elemA First Element
         * @param {string} elemB Second element to be moved in front of first
         */
        swapChildElements: function (childElem, elemA, elemB) {
            var children = jQuery(childElem);
            children.each(function () {
                var child = jQuery(this);
                var a = child.find(elemA);
                var b = child.find(elemB);
                a.before(b);
            })
        },
        /**
         * @description Used to set title tags based off image alt tags in wordpress galleries
         */
        imgTitleFromAltForGallery: function () {
            jQuery("img").each(function () {
                var alt = jQuery(this).attr('alt');
                var title = jQuery(this).attr('title');
                if (!(alt && title)) {
                    if (alt) {
                        jQuery(this).attr("title", alt);
                    } else {
                        jQuery(this).attr("alt", title);
                    }
                }
            });
        }

    }
    ;
