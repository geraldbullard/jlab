<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: scripts.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
    <!-- jQuery 3 -->
    <script src="/public/themes/adminlte2/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/public/themes/adminlte2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--<script src="/public/themes/adminlte2/dist/js/Utils.js"></script>-->
    <!-- FastClick -->
    <script src="/public/themes/adminlte2/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="/public/themes/adminlte2/dist/js/adminlte.js"></script>
    <!-- Sparkline -->
    <script src="/public/themes/adminlte2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap  -->
    <script src="/public/themes/adminlte2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/public/themes/adminlte2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll -->
    <script src="/public/themes/adminlte2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS -->
    <script src="/public/themes/adminlte2/bower_components/chart.js/Chart.js"></script>
    <script src="/public/themes/adminlte2/plugins/jQueryUI/jquery-ui.min.js"></script>
    <script src="/public/ext/jquery-confirm/jquery-confirm.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/public/themes/adminlte2/dist/js/theme.js"></script>
    <!-- Save if needed
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/public/themes/adminlte/plugins/moment/moment.min.js"></script>
    <script src="/public/themes/adminlte/plugins/daterangepicker/daterangepicker.js"></script>-->
    <!-- <script>$.widget.bridge('uibutton', $.ui.button);</script>
    <script src="/public/themes/adminlte/plugins/raphael/raphael.min.js"></script>
    <script src="/public/themes/adminlte/plugins/morris/morris.min.js"></script>
    <script src="/public/themes/adminlte/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="/public/themes/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/public/themes/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/public/themes/adminlte/plugins/knob/jquery.knob.js"></script>
    <script src="/public/themes/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="/public/themes/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="/public/themes/adminlte/plugins/fastclick/fastclick.min.js"></script>
    <script src="/public/themes/adminlte/plugins/select2/select2.full.min.js"></script> -->
    <!-- Ckeditor js
    <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js"></script>
    <script src="/public/themes/adminlte/dist/js/app.min.js"></script> -->
    <?php
    if (
        isset($_SESSION[Session::get('current_view')]['footerJsInclude']) &&
        count($_SESSION[Session::get('current_view')]['footerJsInclude']) > 0
    ) {
        foreach ($_SESSION[Session::get('current_view')]['footerJsInclude'] as $i => $js) {
    ?>
    <script src="<?php echo $js; ?>"></script>
    <?php
        }
    }
    ?>
    <!-- Sortable js -->
    <script src="/public/ext/jquery-sortable/sortable.js"></script>
    <script>
        $("[data-widget='collapse']").click(function() {
            //Find the box parent........
            var box = $(this).parents(".box").first();
            //Find the body and the footer
            var bf = box.find(".box-body, .box-footer");
            if (!$(this).children().hasClass("fa-plus")) {
                bf.slideUp();
                $(this).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
            } else {
                bf.slideDown();
                $(this).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            }
        });
        /**
         * @description PHP-like print_r() & var_dump() equivalent for JavaScript Object
         * @author Faisalman - movedpixel@gmail.com
         * @license http://www.opensource.org/licenses/mit-license.php
         * @link http://gist.github.com/879208
         *
         * examples:
         *   // alert a boolean value
         *   alert(Utils.isDevice("TABLET"));
         *   // alert an object/array
         *   alert(Utils.print_r(Utils.device));
         */
        function print_r(obj) {
            // define tab spacing
            var tab = '';
            // check if it's array
            var isArr = Object.prototype.toString.call(obj) === '[object Array]' ? true : false;
            // use {} for object, [] for array
            var str = isArr ? ('Array [' + tab + '\n') : ('Object {' + tab + '\n');
            // walk through it's properties
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    var val1 = obj[prop];
                    var val2 = '';
                    var type = Object.prototype.toString.call(val1);
                    switch (type) {
                        // recursive if object/array
                        case '[object Array]':
                        case '[object Object]':
                            val2 = print_r(val1, (tab + '\t'));
                            break;
                        case '[object String]':
                            val2 = '\'' + val1 + '\'';
                            break;
                        default:
                            val2 = val1;
                    }
                    str += tab + '\t' + prop + ' => ' + val2 + ',\n';
                }
            }
            // remove extra comma for last property
            str = str.substring(0, str.length - 2) + '\n' + tab;
            return isArr ? (str + ']') : (str + '}');
            var var_dump = print_r; // equivalent function
        }

        $(document).ready(function () {
            // fadse out the page overlay if visible
            $('#overlay').fadeOut(2000);
            // admin menu click toggle sub menu if exists
            $('li.treeview').on('click', function() {
                if ($(this).find("ul.treeview-menu").length > 0) {
                    $(this).addClass("menu-open");
                    if ($(this).find(".fa-angle-right").length > 0) {
                        $(this).find(".fa-angle-right").addClass("fa-angle-down");
                        $(this).find(".fa-angle-right").removeClass("fa-angle-right");
                    } else if ($(this).find(".fa-angle-down").length > 0) {
                        $(this).find(".fa-angle-down").addClass("fa-angle-right");
                        $(this).find(".fa-angle-down").removeClass("fa-angle-down");
                    }
                    $(this).find("ul.treeview-menu").toggle(400);
                }
            });
        });
    </script>
