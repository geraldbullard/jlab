<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: footer.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
    </div>
    <footer>
        <div class="container">
            <hr>
            <div class="row">
                <div class="col-sm-12 text-center">
                    &copy; <?php echo date("Y"); ?> gnsCMS. All Rights Reserved.
                </div>
            </div>
        </div>
    </footer>
