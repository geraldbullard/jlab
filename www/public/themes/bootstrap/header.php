<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: header.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<!DOCTYPE html>
<html lang="<?php echo $Options['core']['defaultLang']; ?>">
<head>
    <title><?php echo $Content['title'] . ' - ' . $Options['design']['siteName']; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/public/themes/bootstrap/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/themes/bootstrap/assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/public/themes/bootstrap/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/public/themes/bootstrap/assets/css/jquery-confirm.min.css">
    <link rel="stylesheet" href="/public/themes/bootstrap/assets/css/custom.css">
    <link rel="stylesheet" href="/public/themes/bootstrap/assets/css/simple-slider.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <!--[if lt IE 7]>
    <link href="/public/themes/bootstrap/assets/css/font-awesome-ie7.min.css" rel="stylesheet">
    <![endif]-->
    <script src="/public/themes/bootstrap/assets/js/jquery.min.js"></script>
    <script src="/public/themes/bootstrap/assets/js/jquery-migrate.min.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!--[if lt IE 9]>
    <script src="/public/themes/bootstrap/assets/js/html5.js" type="text/javascript"></script>
    <![endif]-->
    <link rel="shortcut icon"
          href="/public/themes/bootstrap/assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="/public/themes/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="/public/themes/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="/public/themes/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed"
          href="/public/themes/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar-header">
                        <button
                            type="button"
                            class="navbar-toggle"
                            data-toggle="collapse"
                            data-target=".navbar-ex1-collapse">
                            <span class="sr-only"><?php echo $Language->get('text_toggle_navigation'); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/index.html">
                            <img src="/public/images/admin_logo.png" alt="admin-logo" title="<?php
                                echo $Options['design']['siteName'];
                            ?>" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (Session::get('loggedIn') == true) {
                            if (Session::get('admin_index') >= ACCESS_VIEW) {
                            ?>
                            <li>
                                <a href="/admin">
                                    <?php echo $Language->get('text_admin'); ?>
                                </a>
                            </li>
                            <?php
                            }
                            if (Session::get('user_index') >= ACCESS_VIEW) {
                            ?>
                            <li class="dropdown">
                                <a data-toggle="dropdown" href="javascript:;">
                                    <?php echo $Language->get('text_users'); ?> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php
                                    if (Session::get('user_index') > ACCESS_VIEW) {
                                    ?>
                                    <li>
                                        <a href="/user">
                                            <?php
                                            echo $Language->get('text_existing') . ' ' . $Language->get('text_users');
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                    }
                                    if (Session::get('user_create') > ACCESS_VIEW) {
                                    ?>
                                    <li>
                                        <a href="/user/createuser">
                                            <?php
                                            echo $Language->get('text_create') . ' ' . $Language->get('text_new') .
                                                ' ' . $Language->get('text_user');
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                            }
                            if (Session::get('roles_index') >= ACCESS_VIEW) {
                            ?>
                            <li class="dropdown">
                                <a data-toggle="dropdown" href="javascript:;">
                                    <?php echo $Language->get('text_roles'); ?> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php
                                    if (Session::get('roles_index') > ACCESS_VIEW) {
                                    ?>
                                    <li>
                                        <a href="/roles">
                                            <?php
                                            echo $Language->get('text_existing') . ' ' . $Language->get('text_roles');
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                    }
                                    if (Session::get('roles_create') > ACCESS_VIEW) {
                                    ?>
                                    <li>
                                        <a href="/roles/createrole">
                                            <?php
                                            echo $Language->get('text_create') . ' ' . $Language->get('text_new') .
                                                ' ' . $Language->get('text_role');
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                            }
                            if (Session::get('options_index') >= ACCESS_VIEW) {
                            ?>
                            <li class="dropdown">
                                <a data-toggle="dropdown" href="javascript:;">
                                    <?php echo $Language->get('text_options'); ?> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php
                                    if (Session::get('options_index') > ACCESS_VIEW) {
                                        foreach(Option::getOptionTypes() as $o => $option_type) {
                                    ?>
                                    <li>
                                        <a href="/options/?type=<?php echo $option_type['type']; ?>">
                                            <?php
                                            echo ucfirst($option_type['type']) . ' ' . $Language->get('text_options');
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <li>
                                        <a href="/options">
                                            <?php
                                            echo $Language->get('text_all') . ' ' . $Language->get('text_options');
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                    }
                                    if (Session::get('options_create') > ACCESS_VIEW) {
                                    ?>
                                    <li>
                                        <a href="/options/createoption">
                                            <?php
                                            echo $Language->get('text_create') . ' ' . $Language->get('text_new') .
                                                ' ' . $Language->get('text_option');
                                            ?>
                                        </a>
                                    </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                            }
                            if (Session::get('modules_index') >= ACCESS_VIEW) {
                            ?>
                            <li>
                                <a href="/modules">
                                    <?php echo $Language->get('text_modules'); ?>
                                </a>
                            </li>
                            <?php
                            }
                            ?>
                            <li>
                                <a href="/logout/run">
                                    <?php echo $Language->get('text_logout'); ?>
                                </a>
                            </li>
                        <?php
                        } else {
                            if ($Modules['help']['status'] == 1) {
                        ?>
                            <li>
                                <a href="/help">
                                    <?php echo $Language->get('text_help'); ?>
                                </a>
                            </li>
                            <?php
                            }
                            ?>
                            <li>
                                <a href="/login">
                                    <?php echo $Language->get('text_login'); ?>
                                </a>
                            </li>
                        <?php
                        }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
