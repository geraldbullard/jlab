<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: scripts.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
    <script src="/public/themes/bootstrap/assets/js/jquery-confirm.min.js"></script>
    <!--<script src="/public/themes/bootstrap/assets/js/bootstrap-tooltip.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-transition.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-alert.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-modal.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-dropdown.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-scrollspy.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-tab.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-popover.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-button.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-collapse.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-carousel.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-typeahead.js"></script>
    <script src="/public/themes/bootstrap/assets/js/bootstrap-affix.js"></script>-->
    <script src="/public/themes/bootstrap/assets/js/simple-slider.js"></script>
