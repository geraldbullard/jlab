<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: footer.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/himu/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
    </div>
</section>
<footer id="footer">
    <div class="container">
        <div class="text-center">
            <p>Copyright &copy; <?php echo date("Y"); ?> <a href="http://www.brokenpixelcms.com/" target="_blank">
                BrokenPixel</a>. All Rights Reserved</p>
        </div>
    </div>
</footer>