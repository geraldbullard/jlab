<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: functions.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/himu/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;

// get the theme menu ul li list
function getDefaultMenu($parent = 0) {
    $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
    $results =  $conn->select(
        "SELECT `id`, `title`, `slug`, `sort`, `parent` type FROM " . DB_PREFIX . "content WHERE parent = " . (int)$parent . " AND `status` = 1 ORDER BY `sort` ASC"
    );
    $items = array();
    foreach ($results as $result) {
        $items[] = '<li><a href="' . $result['slug'] . '.html">' . $result['title'] . '</a>' . /*getDefaultMenu($result['id']) .*/ '</li>';
    }
    if (count($items)) {
        return '<ul class="nav navbar-nav navbar-right">' . implode('', $items) . '</ul>';
    } else {
        return '';
    }
}

function getTopLevelMenuLi() {
    $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
    $results =  $conn->select(
        "SELECT `id`, `title`, `slug`, `sort`, `parent` type FROM " . DB_PREFIX . "content WHERE parent = 0 AND `status` = 1 ORDER BY `sort` ASC"
    );
    $items = array();
    foreach ($results as $result) {
        $items[] = '<li><a href="' . $result['slug'] . '.html">' . $result['title'] . '</a></li>';
    }
    if (count($items)) {
        return implode('', $items);
    } else {
        return '';
    }
}
