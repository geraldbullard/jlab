<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: header.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/himu/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
    /*echo '<pre>[';
    print_r($this->getContent()['id']);
    echo ']</pre>';
    die();*/
// get the theme specific functions this way until the core does the work later
include_once('functions.php');
?>
<!DOCTYPE html>
<html lang="<?php echo $Options['core']['defaultLang']; ?>">
<head>
    <title><?php echo $this->getContent()['title'] . ' - ' . $Options['core']['siteName']; ?></title>
    <meta charset="<?php echo $Options['core']['defaultCharSet']; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if (isset($this->getContent()['id']) && $this->getContent()['id'] != '') { ?>
    <meta name="description" content="<?php echo $this->getContent()['metaDescription']; ?>">
    <meta name="keywords" content="<?php echo $this->getContent()['metaKeywords']; ?>">
    <?php } ?>
    <meta name="author" content="<?php echo $Options['core']['siteAuthor']; ?>">
    <link href="/public/themes/himu/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/themes/himu/css/prettyPhoto.css" rel="stylesheet">
    <link href="/public/themes/himu/css/font-awesome.min.css" rel="stylesheet">
    <link href="/public/themes/himu/css/animate.css" rel="stylesheet">
    <link href="/public/themes/himu/css/main.css" rel="stylesheet">
    <link href="/public/themes/himu/css/responsive.css" rel="stylesheet">
    <link href="/public/themes/himu/css/bp-custom.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/public/themes/himu/js/html5shiv.js"></script>
    <script src="/public/themes/himu/js/respond.min.js"></script>
    <![endif]-->
    <link href="/public/themes/himu/images/ico/favicon.png" rel="shortcut icon">
    <link href="/public/themes/himu/images/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
    <link href="/public/themes/himu/images/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link href="/public/themes/himu/images/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link href="/public/themes/himu/images/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed">
</head>
<body<?php echo (isset($_SESSION['usersRoleId']) && $_SESSION['usersRoleId'] < 3 ? ' class="admin-session"' : ''); ?>>
<div class="preloader">
    <div class="preloder-wrap">
        <div class="preloder-inner">
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
        </div>
    </div>
</div>
<header id="navigation">
    <div class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"><?php echo $Language->get('text_toggle_navigation'); ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/index.html"><h1><img src="/public/themes/himu/images/logo.png" alt="logo"></h1></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <?php echo getTopLevelMenuLi(); ?>
                    <li class="scroll"><a href="/top-level-category/test-second-level-page/test-third-level-page/test-fourth-level-page.html">4th Level Page</a></li>
                    <?php if (!isset($_SESSION['usersRoleId'])) { ?>
                    <li class="scroll"><a href="/login">Login</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</header>
<?php if (@$Content['siteIndex']) { ?>
<section id="home">
    <div class="home-pattern"></div>
    <div id="main-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#main-carousel" data-slide-to="1"></li>
            <li data-target="#main-carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active" style="background-image: url('/public/themes/himu/images/slider/slide3.jpg')">
                <div class="carousel-caption">
                    <div>
                        <h2 class="heading animated bounceInDown">Himu Responsive Bootstrap Template</h2>
                        <p class="animated bounceInUp">Fully Professional Responsive Bootstrap Template</p>
                        <a class="btn btn-default slider-btn animated fadeIn" href="#">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url('/public/themes/himu/images/slider/slide2.jpg')">
                <div class="carousel-caption"> <div>
                        <h2 class="heading animated bounceInDown">Get Everything You Need In One Template</h2>
                        <p class="animated bounceInUp">Everything is Outstanding </p>
                        <a class="btn btn-default slider-btn animated fadeIn" href="#">Get Started</a>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url('/public/themes/himu/images/slider/slide1.jpg')">
                <div class="carousel-caption">
                    <div>
                        <h2 class="heading animated bounceInRight">Fully Responsive Bootstrap Template</h2>
                        <p class="animated bounceInLeft">100% Responsive Bootstrap Template</p>
                        <a class="btn btn-default slider-btn animated bounceInUp" href="#">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-left member-carousel-control hidden-xs" href="#main-carousel" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="carousel-right member-carousel-control hidden-xs" href="#main-carousel" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</section>
<?php } ?>
<section id="main-content">
    <div class="container">
