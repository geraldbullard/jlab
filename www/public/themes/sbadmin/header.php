<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: header.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<!DOCTYPE html>
<html lang="<?php echo $Options['core']['defaultLang']; ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo 'Module Name' . ' - ' . $Options['design']['siteName']; ?></title>
    <link rel="stylesheet" href="/public/themes/sbadmin/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/themes/sbadmin/css/metisMenu.min.css">
    <link rel="stylesheet" href="/public/themes/sbadmin/css/morris.css">
    <link rel="stylesheet" href="/public/themes/sbadmin/css/dataTables.css">
    <link rel="stylesheet" href="/public/themes/sbadmin/css/dataTablesResponsive.css">
    <link rel="stylesheet" href="/public/themes/sbadmin/css/bootstrapSocial.css">
    <link rel="stylesheet" href="/public/themes/sbadmin/css/rangeslider.css">
    <link rel="stylesheet" href="/public/themes/sbadmin/css/sb-admin-2.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/public/css/AlertSet.css" />
    <link rel="stylesheet" href="/public/css/CalendarSet.css" />
    <link rel="stylesheet" href="/public/css/dropzone.css" />
    <?php if (!Session::get('loggedIn')) { ?>
    <link rel="stylesheet" href="/public/themes/sbadmin/css/login.css" />
    <?php } ?>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- ****** faviconit.com favicons ****** -->
    <link rel="shortcut icon" href="/public/images/favicon.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href="/public/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="196x196" href="/public/images/favicon-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="/public/images/favicon-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/public/images/favicon-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/public/images/favicon-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/public/images/favicon-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/public/images/favicon-16.png">
    <link rel="apple-touch-icon" href="/public/images/favicon-57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/public/images/favicon-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/public/images/favicon-72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/public/images/favicon-144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/public/images/favicon-60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/public/images/favicon-120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/public/images/favicon-76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/public/images/favicon-152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/public/images/favicon-180.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="/public/images/favicon-144.png">
    <meta name="msapplication-config" content="/public/images/browserconfig.xml">
    <!-- ****** faviconit.com favicons ****** -->
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"><?php echo $Language->get('text_toggle_navigation'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/index" title="<?php echo $Options['design']['siteName']; ?>"><img
                src="/public/images/admin_logo.png"
                alt="<?php echo $Options['design']['siteName']; ?>"
            /></a>
        </div>
        <?php //if (Session::get('loggedIn')) { ?>
            <ul class="nav navbar-top-links navbar-right">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input class="form-control" placeholder="<?php
                        echo $Language->get('text_search');
                        ?>..." type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </li>
                <!--<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>-->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="/user/edit/<?php //P::out($currentUser->getId()); ?>">
                                <i class="fa fa-user fa-fw"></i> Edit Profile
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/logout/run"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="/admin" title="<?php echo $Language->get('text_dashboard'); ?>">
                                <i class="fa fa-dashboard fa-fw"></i>
                                <span><?php echo $Language->get('text_dashboard'); ?></span>
                            </a>
                        </li>
                        <?php
                        if (Session::get('loggedIn')) {
                            if (!empty($Modules)) {
                                foreach ($Modules as $a_module) {
                                    if ($a_module['theme'] == 'admin' && $a_module['module'] != 'admin') {
                                        // later we will add a check for the module status (active/inactive)
                                        //if (@$currentUser->tabPermission($a_module, 'view') && @$a_module->view == 'admin' && @$a_module->status == '1' && @$a_module->view != 'hidden') {
                                        ?>
                                        <li>
                                            <a href="javascript:void(0);"
                                               id="<?php echo $a_module['module']; ?>_module"
                                               title="<?php echo $a_module['title']; ?>">
                                                <i class="fa <?php echo $a_module['icon']; ?> fa-fw"></i>
                                                <?php echo $a_module['title']; ?>
                                                    <span class="fa fa-angle-right"></span>
                                            </a>
                                            <ul class="nav nav-second-level">
                                                <li>
                                                    <a href="/<?php echo $a_module['module']; ?>"><i class="fa <?php
                                                        echo $a_module['icon']; ?> fa-fw"></i><?php
                                                        echo $a_module['title'];
                                                    ?></a>
                                                </li>
                                            </ul>
                                            <?php
                                            /*if (!empty($a_module->tabs)) {
                                                ?>
                                                <ul class="nav nav-second-level">
                                                    <?php
                                                    if ($a_module->name != 'bootstrap') { // hide bootstrap view menu
                                                        ?>
                                                        <li>
                                                            <a href="/<?php P::out($a_module->name) ?>" title="<?php P::out($a_module->title) ?>">
                                                                <i class="fa <?php P::out($a_module->icon) ?> fa-fw"></i>
                                                                <span><?php P::out($a_module->title) ?></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    foreach ($a_module->tabs as $a_tab) {
                                                        if ($a_tab->hidden) {
                                                            continue;
                                                        }
                                                        ?>
                                                        <li>
                                                            <a href="/<?php P::out($a_module->name) ?>/<?php P::out($a_tab->name) ?>" title="<?php P::out($a_tab->title) ?>">
                                                                <i class="fa <?php P::out($a_tab->icon) ?> fa-fw"></i> <span><?php P::out($a_tab->title) ?></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                                <?php
                                            }*/
                                            ?>
                                        </li>
                                        <?php
                                        //}
                                    }
                                }
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        <?php //} ?>
    </nav>
    <div id="page-wrapper">

    <!--<div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav navbar-right">
        <?php
        //if (Session::get('loggedIn') == true) {
            //if (Session::get('admin_index') >= ACCESS_VIEW) {
            ?>
            <li>
                <a href="/admin">
                    <?php //echo $Language->get('text_admin'); ?>
                </a>
            </li>
            <?php
            //}
            //if (Session::get('user_index') >= ACCESS_VIEW) {
            ?>
            <li class="dropdown">
                <a data-toggle="dropdown" href="javascript:;">
                    <?php //echo $Language->get('text_users'); ?> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <?php
                    //if (Session::get('user_index') > ACCESS_VIEW) {
                    ?>
                    <li>
                        <a href="/user">
                            <?php
                            //echo $Language->get('text_existing') . ' ' . $Language->get('text_users');
                            ?>
                        </a>
                    </li>
                    <?php
                    //}
                    //if (Session::get('user_create') > ACCESS_VIEW) {
                    ?>
                    <li>
                        <a href="/user/createuser">
                            <?php
                            //echo $Language->get('text_create') . ' ' . $Language->get('text_new') .
                                //' ' . $Language->get('text_user');
                            ?>
                        </a>
                    </li>
                    <?php
                    //}
                    ?>
                </ul>
            </li>
            <?php
            //}
            //if (Session::get('roles_index') >= ACCESS_VIEW) {
            ?>
            <li class="dropdown">
                <a data-toggle="dropdown" href="javascript:;">
                    <?php //echo $Language->get('text_roles'); ?> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <?php
                    //if (Session::get('roles_index') > ACCESS_VIEW) {
                    ?>
                    <li>
                        <a href="/roles">
                            <?php
                            //echo $Language->get('text_existing') . ' ' . $Language->get('text_roles');
                            ?>
                        </a>
                    </li>
                    <?php
                    //}
                    //if (Session::get('roles_create') > ACCESS_VIEW) {
                    ?>
                    <li>
                        <a href="/roles/createrole">
                            <?php
                            //echo $Language->get('text_create') . ' ' . $Language->get('text_new') .
                                //' ' . $Language->get('text_role');
                            ?>
                        </a>
                    </li>
                    <?php
                    //}
                    ?>
                </ul>
            </li>
            <?php
            //}
            //if (Session::get('options_index') >= ACCESS_VIEW) {
            ?>
            <li class="dropdown">
                <a data-toggle="dropdown" href="javascript:;">
                    <?php //echo $Language->get('text_options'); ?> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <?php
                    //if (Session::get('options_index') > ACCESS_VIEW) {
                        //foreach(Option::getOptionTypes() as $o => $option_type) {
                    ?>
                    <li>
                        <a href="/options/?type=<?php //echo $option_type['type']; ?>">
                            <?php
                            //echo ucfirst($option_type['type']) . ' ' . $Language->get('text_options');
                            ?>
                        </a>
                    </li>
                    <?php
                        //}
                    ?>
                    <li>
                        <a href="/options">
                            <?php
                            //echo $Language->get('text_all') . ' ' . $Language->get('text_options');
                            ?>
                        </a>
                    </li>
                    <?php
                    //}
                    //if (Session::get('options_create') > ACCESS_VIEW) {
                    ?>
                    <li>
                        <a href="/options/createoption">
                            <?php
                            //echo $Language->get('text_create') . ' ' . $Language->get('text_new') .
                                //' ' . $Language->get('text_option');
                            ?>
                        </a>
                    </li>
                    <?php
                    //}
                    ?>
                </ul>
            </li>
            <?php
            //}
            //if (Session::get('modules_index') >= ACCESS_VIEW) {
            ?>
            <li>
                <a href="/modules">
                    <?php //echo $Language->get('text_modules'); ?>
                </a>
            </li>
            <?php
            //}
            ?>
            <li>
                <a href="/logout/run">
                    <?php //echo $Language->get('text_logout'); ?>
                </a>
            </li>
        <?php
        //} else {
            //if ($Modules['help']['status'] == 1) {
        ?>
            <li>
                <a href="/help">
                    <?php //echo $Language->get('text_help'); ?>
                </a>
            </li>
            <?php
            //}
            ?>
            <li>
                <a href="/login">
                    <?php //echo $Language->get('text_login'); ?>
                </a>
            </li>
        <?php
        //}
        ?>
        </ul>
    </div>-->