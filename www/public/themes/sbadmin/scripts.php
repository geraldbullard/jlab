<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: scripts.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="/public/js/jquery-ui.min.js"></script>
    <script src="/public/themes/sbadmin/js/bootstrap.min.js"></script>
    <script src="/public/themes/sbadmin/js/metisMenu.min.js"></script>
    <script src="/public/themes/sbadmin/js/sb-admin-2.js"></script>
    <script src="/public/js/dropzone.js"></script>
    <script src="/public/js/util.js"></script>
    <script src="/public/js/Ajax.js"></script>
    <script src="/public/js/json2.js"></script>
    <script src="/public/js/AlertSet.js"></script>
    <script src="/public/js/Tabmin.js"></script>
    <script src="/public/js/CalendarSet.js"></script>
    <script src="/public/ckeditor/ckeditor.js"></script>
    <script src="/public/themes/sbadmin/js/raphael.min.js"></script>
    <script src="/public/themes/sbadmin/js/morris.min.js"></script>
    <script src="/public/themes/sbadmin/js/morris-data.js"></script>
    <script>
        // tooltip demo
        $('.tooltip-demo').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
        // popover demo
        $("[data-toggle=popover]").popover();
        $(document).ready(function(){
            // admin side menu arrows
            $('#side-menu > li > a').each(function() {
                var parentClass = $(this).parent().attr("class");
                if (parentClass == 'active') {
                    $("#" + this.id + " span span.fa-angle-right").removeClass("fa-angle-right").addClass("fa-angle-down");
                }
                $(this).click(function(){
                    var thisClosed = $("#" + this.id + " .fa-angle-right").length;
                    $('#side-menu > li > a').each(function() {
                        $("#side-menu > li > a > span > span.fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-right");
                    });
                    if (thisClosed) {
                        $("#" + this.id + " span span.fa-angle-right").removeClass("fa-angle-right").addClass("fa-angle-down");
                    } else {
                        $("#" + this.id + " span span.fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-right");
                    }
                });
            });
            setTimeout(function(){
                // wait 8 seconds and remove success alerts
                $(".alert.alert-dismissable").fadeOut(2000, function(){
                    // Animation complete, if anything to do, do it here
                });
            }, 8000);
        });
        //CKEDITOR.replace('editor1', {
        //height: 260 // custom height
        //});
    </script>
    <script src="/public/themes/sbadmin/js/rangeslider.min.js"></script>
    <script>
        $(function() {
            var $document   = $(document),
                $inputRange = $('input[type="range"]');
            function valueOutput(element) {
                var value = element.value,
                    output = element.parentNode.getElementsByTagName('output')[0];
                if (value == 0)
                    output.innerHTML = 'Level 0: None';
                if (value == 1)
                    output.innerHTML = 'Level 1: View';
                if (value == 2)
                    output.innerHTML = 'Level 2: Edit';
                if (value == 3)
                    output.innerHTML = 'Level 3: Add';
                if (value == 4)
                    output.innerHTML = 'Level 4: Delete';
            }
            for (var i = $inputRange.length - 1; i >= 0; i--) {
                valueOutput($inputRange[i]);
            };
            $document.on('input', 'input[type="range"]', function(e) {
                valueOutput(e.target);
            });
            $inputRange.rangeslider({
                polyfill: false
            });
        });
    </script>
